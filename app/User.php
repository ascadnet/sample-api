<?php

namespace App;

//use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
//use Illuminate\Auth\Passwords\CanResetPassword;
//use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
//use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

/**
 * This is the primary user table within the database. All profiles are created
 * under the umbrella of a user.
 *
 * @param   status  1 = active, 0 = suspended, 3 = unclaimed
 *
 * @date    2015-06-19
 * @author  j-belelieu
 */
class User extends Model // implements AuthenticatableContract, CanResetPasswordContract
{
    //use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status',
        'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];



    public function profiles()
    {
        return $this->hasMany('\App\UserProfile', 'user_id', 'id');
    }

    public function devices()
    {
        return $this->hasMany('\App\UserDevices', 'user_id', 'id');
    }

    public function social()
    {
        return $this->hasMany('\App\UserSocial', 'user_id', 'id');
    }

}
