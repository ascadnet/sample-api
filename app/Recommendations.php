<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recommendations extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'recommendations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'recommends',
        'reason',
        'super_category_id',
        'category_id',
        'neighborhood_id',
        'posted_by',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var     array
     */
    protected $hidden = [];




    public function profile()
    {
        return $this->belongsTo('\App\UserProfile', 'recommends', 'id');
    }

    public function owner()
    {
        return $this->belongsTo('\App\UserProfile', 'posted_by', 'id');
    }

    public function neighborhood()
    {
        return $this->hasOne('\App\GeoNeighborhoods', 'id', 'neighborhood_id');
    }

    public function category()
    {
        return $this->hasOne('\App\Categories', 'id', 'category_id');
    }

    public function media()
    {
        return $this->hasMany('\App\UserMedia', 'recommendation_id', 'id');
    }

}
