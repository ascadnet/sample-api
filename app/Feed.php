<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'feed';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date',
        'posted_by',
        'type',
        'session',
        'serv_id',
        'recc_id',
        'deal_id',
        'ques_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var     array
     */
    protected $hidden = [];

}
