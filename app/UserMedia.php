<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Stores all media associated with a user's profile.
 *
 * @param   type            Mainly for images.
 *                          1 = primary profile / 2 = secondary
 * @param   media_type      1 = image / 2 = video
 *
 * @date    2015-06-19
 * @author  j-belelieu
 */
class UserMedia extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_media';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'profile_id',
        'raw_image_path',
        'format',
        'type',
        'media_type',
        'recommendation_id',
        'service_id',
        'question_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


    public function profile()
    {
        return $this->belongsTo('\App\UserProfile', 'id', 'profile_id');
    }

}
