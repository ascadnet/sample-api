<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Controls whether a profile is currently an expert in a neighborhood
 * and/or category.
 *
 * @param   type                1 = Community (Neighborhood) / 2 = Industry
 * @param   neighborhood_id
 * @param   category_id
 *
 * @date    2015-06-19
 * @author  j-belelieu
 */
class UserExperts extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_experts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'profile_id',
        'type',
        'neighborhood_id',
        'category_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


    public function profile()
    {
        return $this->belongsTo('\App\UserProfile', 'id', 'profile_id');
    }

    public function neighborhood()
    {
        return $this->hasOne('\App\GeoNeighborhoods', 'id', 'neighborhood_id');
    }

    public function category()
    {
        return $this->hasOne('\App\Categories', 'id', 'category_id');
    }

}
