<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Information on who is following what.
 *
 * @param   follow_type         1 = profile / 2 = neighborhood / 3 = city
 * @param   follow_profile_id   ID of the profile this profile is following.
 *                                 Related to: user_profile.id
 * @param   neighborhood_id     ID of the neighborhood this profile is following.
 *                                 Related to: geo_neighborhoods.id
 * @param   city_id             ID of the city this profile is following.
 *                                 Related to: geo_metro.id
 * @param   status              1 = following / 0 = unfollowed
 *
 * @date    2015-06-19
 * @author  j-belelieu
 */
class UserFollows extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_follows';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'follow_type',
        'profile_id',
        'follow_id',
        'neighborhood_id',
        'category_id',
        'question_id',
        'status',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


    public function profile()
    {
        return $this->belongsTo('\App\UserProfile', 'id', 'profile_id');
    }

}
