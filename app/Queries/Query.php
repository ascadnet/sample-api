<?php namespace App\Queries;

/**
 * Simple abstraction layer for querying the various databases.
 * 
 * @author  j-belelieu
 * @date    6/23/15
 */

class Query {

    protected $query_id;

    protected $result;

    protected $error = false;
    protected $errorCode;

    protected $sql;


    public function __construct($id = '')
    {
        if (! empty($id)) $this->setQuery($id);
    }


    public function setQuery($id)
    {
        $this->query_id = $id;
    }


    public function getResult()
    {
        return $this->result;
    }


    public function getSql()
    {
        return $this->result->toSql();
    }

    public function run()
    {
        $arguments = func_get_args();

        $class = 'App\Queries\Mappings\\' . $this->query_id;

        if (class_exists($class)) {
            $queryClass = new $class();
            $queryClass->setData($arguments);
            $this->result = $queryClass->run()->get();
        } else {
            $this->error = true;
            $this->errorCode = 'E01';
            $this->sql = $this->getSql();
        }

        return $this;
    }

}