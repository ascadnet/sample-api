<?php namespace App\Queries;

/**
 *
 * 
 * 
 * @author  j-belelieu
 * @date    6/23/15
 */

class BaseQuery {


    protected $data;

    protected $connection = 'mysql';

    protected $arguments = [];

    protected $error = false;

    protected $errorMessage = '';

    //protected $fillableValues = [];

    //protected $finalInput = [];



    /**
     * @param array $arguments
     */
    public function setData(array $arguments)
    {
        $this->arguments = $arguments;
    }


    /**
     * This ensures that the input array has all of the required
     * parameters to run the requested query.
     *
     * @param   array   $input
     *
     * @return  object
     */
    /*
    public function syncInput($input)
    {
        foreach ($input as $key => $value) {
            if (! array_key_exists($key, $this->fillableValues) && $key != 'key') {
                $this->finalInput[$key] = $value;
            }
        }

        foreach ($this->fillableValues as $key) {
            if (isset($input[$key])) {
                $this->finalInput[$key] = $input[$key];
            } else {
                $this->finalInput[$key] = '';
            }
        }

        return $this;
    }
    */


    /**
     * @param   string  $key
     * @param   string  $value
     */
    /*
    public function setFillableDefault($key, $value)
    {
        $this->finalInput[$key] = $value;
    }
    */


    /**
     * @return array
     */
    /*
    public function getInput()
    {
        return $this->finalInput;
    }
    */


    /**
     * @return mixed
     */
    public function get()
    {
        if ($this->error) {
            return false;
            /*
            return [
                'error' => true,
                'errorMessage' => $this->errorMessage,
            ];
            */
        }

        // Happens when a item is created (for example)
        // and only the primary ID is returned as data.
        if (! is_object($this->data))
            return $this->data;

        $return = $this->data->get();

        if (sizeof($return) == 1)
            return $return['0'];

        return $return;
    }

}