<?php
namespace App\Queries;

/**
 * 
 * 
 * @author  j-belelieu
 * @date    6/23/15
 */

interface QueryContract {

    public function run();
    public function get();

}