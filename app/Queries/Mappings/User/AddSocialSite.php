<?php namespace App\Queries\Mappings\User;

/**
 * 
 * 
 * @author  j-belelieu
 * @date    6/23/15
 */

use App\Queries\BaseQuery;
use App\Queries\QueryContract;

use App\UserSocial;


class AddSocialSite extends BaseQuery implements QueryContract {


    /**
     *
     */
    public function run()
    {
        return UserSocial::create($this->arguments['0']);
    }

}