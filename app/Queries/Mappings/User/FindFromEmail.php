<?php namespace App\Queries\Mappings\User;

/**
 * 
 * 
 * @author  j-belelieu
 * @date    6/23/15
 */

use App\Queries\BaseQuery;
use App\Queries\QueryContract;

class FindFromEmail extends BaseQuery implements QueryContract {


    /**
     *
     */
    public function run()
    {
        $this->data = \DB::connection($this->connection)
            ->table('user_profile')
            ->select('user_id')
            ->where('email', $this->arguments[0]);

        return $this;
    }

}