<?php namespace App\Queries\Mappings\User;

/**
 * 
 * 
 * @author  j-belelieu
 * @date    6/23/15
 */

use App\Queries\BaseQuery;
use App\Queries\QueryContract;

use App\UserProfile;
use App\UserPrivacy;
use App\UserStats;

use App\lib\Image;


class CreateProfile extends BaseQuery implements QueryContract {


    /**
     *
     */
    public function run()
    {
        $input = $this->arguments['0'];

        try {
            $this->data = \DB::transaction(function () use ($input) {

                $profile_id = UserProfile::create($input);

                $input['profile_id'] = $profile_id->id;

                $privacy = UserPrivacy::create([
                    'profile_id' => $profile_id->id,
                    'email'      => 0,
                    'first_name' => 1,
                    'last_name'  => 1,
                    'phone'      => 0,
                ]);

                $stats = UserStats::create([
                    'profile_id' => $profile_id->id,
                ]);

                // Needs a better method
                $image = new Image();
                if (! empty($input['social_site'])) $image->setPath($input['social_site']);
                $image->setProfile($profile_id->id);

                if (! empty($input['profile_pic'])) {
                    $image->setType('profile_pic')
                        ->setPath($input['profile_pic'])
                        ->run();
                }

                if (! empty($input['cover_pic'])) {
                    $image->setType('cover_pic')
                        ->setPath($input['cover_pic'])
                        ->run();
                }

                return $profile_id->id;

            });
        } catch(Exception $e) {
            $this->error = true;
            $this->errorMessage = $e->getMessage();
        }

        return $this;
    }

}