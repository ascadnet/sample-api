<?php namespace App\Queries\Mappings\User;

/**
 * 
 * 
 * @author  j-belelieu
 * @date    6/23/15
 */

use App\Queries\Query;
use App\Queries\BaseQuery;
use App\Queries\QueryContract;

use App\User;
use App\UserSocial;
use App\UserDevices;


class CreateUser extends BaseQuery implements QueryContract {


    /**
     *
     */
    public function run()
    {
        $input = $this->arguments['0'];

        $this->data = \DB::transaction(function () use ($input) {

            $user_id = User::create($input);

            $input['user_id'] = $user_id->id;

            if (! empty($input['social_id']))
                $social_id = UserSocial::create($input);

            if (! empty($input['device_id'])) {
                try {
                    $device_id = (new UserDevices)->addDeviceToExistingUser(
                        $user_id->id,
                        $input['device_id'],
                        $input['device_type']
                    );
                } catch(Exception $e) {
                    $this->error = true;
                    $this->errorMessage = $e->getMessage();
                }
            }

            $profile_data = (new Query('User\CreateProfile'))->run($input)->getResult();

            return [
                'user_id' => $user_id->id,
                'profile_id' => $profile_data,
            ];

        });

        return $this;
    }

}