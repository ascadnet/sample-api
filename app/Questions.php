<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'questions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status',
        'posted_by',
        'super_category_id',
        'category_id',
        'neighborhood_id',
        'question',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var     array
     */
    protected $hidden = [];




    public function answers()
    {
        return $this->hasMany('\App\QuestionAnswers', 'question_id', 'id');
    }

    public function profile()
    {
        return $this->belongsTo('\App\UserProfile', 'posted_by', 'id');
    }

    public function neighborhood()
    {
        return $this->hasOne('\App\GeoNeighborhoods', 'id', 'neighborhood_id');
    }

    public function category()
    {
        return $this->hasOne('\App\Categories', 'id', 'category_id');
    }

    public function media()
    {
        return $this->hasMany('\App\UserMedia', 'question_id', 'id');
    }

}
