<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeoNeighborhoods extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'geo_neighborhoods';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'google_id',
        'market',
        'state',
        'county',
        'name',
        'type',
        'place_',
        'placecode',
        'metro',
        'country',
        'lat',
        'lon',
        'area',
        'metro_id',
        'seo_ref',
        'borough_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var     array
     */
    protected $hidden = [];


    public function inCity($city)
    {
        return $this->select('id')->where('metro_id', $city)->get();
    }

}
