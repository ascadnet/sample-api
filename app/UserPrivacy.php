<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPrivacy extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_privacy';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'profile_id',
        'email',
        'first_name',
        'last_name',
        'phone',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


    public function profile()
    {
        return $this->belongsTo('\App\UserProfile', 'id', 'profile_id');
    }

}
