<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 *
 *
 * @date    2015-06-19
 * @author  j-belelieu
 */
class UserStats extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_stats';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'profile_id',
        'votes_up',
        'votes_down',
        'shares',
        'logins',
        'page_views',
        'questions_asked',
        'questions_answered',
        'deals_added',
        'deals_accepted',
        'users_followed',
        'users_following',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


    public function profile()
    {
        return $this->belongsTo('\App\UserProfile', 'id', 'profile_id');
    }

}
