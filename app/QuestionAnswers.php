<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionAnswers extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'question_answers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question_id',
        'answered_by',
        'recommendation_id',
        'status',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var     array
     */
    protected $hidden = [];




    public function profile()
    {
        return $this->belongsTo('\App\UserProfile', 'answered_by', 'id');
    }

    public function question()
    {
        return $this->belongsTo('\App\Questions', 'question_id', 'id');
    }

    public function recommendations()
    {
        return $this->hasMany('\App\Recommendations', 'id', 'recommendation_id');
    }

    public function scores()
    {
        return $this->hasMany('\App\QuestionAnswerScores', 'answer_id', 'id');
    }

    public function scores_log()
    {
        return $this->hasMany('\App\QuestionAnswerScoreLog', 'answer_id', 'id');
    }

}
