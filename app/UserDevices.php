<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDevices extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_devices';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'device_id',
        'device_type',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var     array
     */
    protected $hidden = [];


    public function profile()
    {
        return $this->belongsTo('\App\User', 'id', 'user_id');
    }


    // --------------------------------------------------------------------------------


    /**
     * @param   int         $user_id
     * @param   string      $device_id
     * @param   string      $device_type        1 = iOS | 2 = Android
     *
     * @return  mixed
     */
    public function addDeviceToExistingUser($user_id, $device_id, $device_type)
    {
        $find = UserDevices::create([
            'user_id' => $user_id,
            'device_id' => $device_id,
            'device_type' => $this->convertDeviceType($device_type),
        ]);

        return $find->id;
    }


    /**
     * @param   string  $inputType
     *
     * @return  int
     */
    protected function convertDeviceType($inputType)
    {
        switch (strtolower($inputType)) {
            case 'android':
                return 2;
            default:
                return 1;
        }
    }

}
