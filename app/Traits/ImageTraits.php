<?php namespace App\Traits;

use App\lib\Image;

/**
 * 
 * 
 * @author  j-belelieu
 * @date    6/23/15
 */

trait ImageTraits {



    /**
     * This is used to process an image that has been uploaded during an API request.
     *
     * @param   int     $act_id         This is the item associated with the action. For example the question ID or the
     *                                  recommendation ID.
     * @param   int     $profile_id     The profile that uploaded the image, or with which it is associated.
     * @param   string  $type           What type of image we are uploading. Common options:
     *                                  'profile_pic','cover_pic','question','recommendation','other'
     * @param   string  $fileName       Name of parameter within the request, not the file name itself.
     *
     * @return  mixed
     */
    protected function storeImage($act_id, $profile_id, $type, $fileName = 'image')
    {
        if ($this->request->hasFile($fileName)) {
            return (new Image())
                ->setActId($act_id)
                ->setMediaType('image')
                ->setType($type)
                ->setProfile($profile_id)
                ->setPath($this->request->file($fileName))
                ->run();
        }

        return null;
    }

}