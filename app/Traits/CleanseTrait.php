<?php namespace App\Traits;



/**
 * 
 * 
 * @author  j-belelieu
 * @date    6/23/15
 */

trait CleanseTrait {

    /**
     * Re-format fields according to site-wide rules.
     *
     * @param   array   $input
     *
     * @return  mixed
     */
    public function cleanseAll($input)
    {
        // Clean the phone number.
        if (! empty($input['phone']))
            $input['phone'] = $this->cleanse->phone($input['phone']);

        unset($input['key']);

        return $input;
    }


}