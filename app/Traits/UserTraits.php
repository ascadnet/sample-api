<?php namespace App\Traits;

use App\Queries\Query;

/**
 * 
 * 
 * @author  j-belelieu
 * @date    6/23/15
 */

trait UserTraits {

    /**
     * These are the basic validation rules for the request to create a new user.
     *
     * Additional type-specific (IE: form vs social registration) rules are
     * available from within the Request class for those requests.
     *
     * @return  array
     */
    public function getUniveralUserCreateRules()
    {
        $arr = [
            'device_id' => 'required',
            'device_type' => 'required|in:android,ios',
            'device_details' => '',

            'social_id' => 'required',
            'social_site' => 'required|in:facebook,twitter,linkedin,google',
        ];

        $profile = $this->standardProfileFields();

        return array_merge($arr, $profile);
    }


    /**
     * Create a new profile for an existing user.
     *
     * @param   int     $user_id    Primary user (owner) of the profile.
     * @param   array   $input      The data we are inputting for this profile.
     *
     * @return  int     Profile ID
     */
    public function createProfile($user_id, array $input)
    {
        $input['user_id'] = $user_id;

        if (empty($input['password'])) {
            $claimCode = \Hash::make(md5(time()) . 'RomioRocksYoYoFuckYa!');
            $input['password'] = $claimCode;
        }

        if (! empty($input['phone'])) $input['phone'] = $this->cleanse->phone($input['phone']);

        $data = (new Query('User\CreateProfile'))
            ->run($input)
            ->getResult();

        return $data;
    }


    /**
     * @return  array
     */
    public function standardProfileFields()
    {
        return [
            'type' => 'in:user,service',
            'headline' => 'required_if:type,service',
            'email' => 'email|required',
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => '',
            'company' => '',
            'gender' => 'in:male,female',
            'description' => '',
            'website' => 'url',
            'neighborhood_id' => 'integer',
        ];
    }

}