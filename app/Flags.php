<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flags extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'flags';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'flagged_by',
        'item_flagged',
        'item_type',
        'reason',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var     array
     */
    protected $hidden = [];



}
