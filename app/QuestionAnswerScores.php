<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionAnswerScores extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'question_answer_scores';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'answer_id',
        'score',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var     array
     */
    protected $hidden = [];




    public function answer()
    {
        return $this->belongsTo('\App\QuestionAnswers', 'answer_id', 'id');
    }


}
