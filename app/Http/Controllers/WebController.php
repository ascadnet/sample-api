<?php namespace App\Http\Controllers;

use App\Http\Requests;

class WebController extends Controller {

    public function betaInterestForm()
    {
        $add = \DB::table('webBetaForm')
            ->insert([
                'email' => (! empty($this->input['email'])) ? $this->input['email'] : '',
                'announce_service' => (! empty($this->input['announce_service'])) ? 1 : 0,
                'expert_learn' => (! empty($this->input['expert_learn'])) ? 1 : 0,
            ]);

        $ref = \Request::server('HTTP_REFERER');

        header('Location: ' . $ref . '?email=true');
        // echo json_encode(['success' => true]);
        exit;
    }

}
