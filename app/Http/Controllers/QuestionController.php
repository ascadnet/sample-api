<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Traits\ImageTraits;
use App\lib\Stats;

class QuestionController extends Controller
{

    use ImageTraits;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $this->error('998');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->error('998');
    }

    /**
     * Ask a question.
     *
     * @todo    Validate that category exists.
     * @todo    Remove profanities.
     *
     * @return Response
     */
    public function store(\App\Http\Requests\QuestionCreateRequest $request)
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0');

        //$master_category = (new \App\Categories)->getSuperCategory($this->input['category_id']);
        //$this->input['super_category_id'] = $master_category;
        $this->input['status'] = '1';
        $this->input['posted_by'] = $this->apiSession->profile->id;

        $add = \App\Questions::create($this->input);

        $this->storeImage($add->id, $this->apiSession->profile->id, 'question');

        $this->after($add->id, 'question.create');

        $this->pushToFeed($add->id, 'question');

        $stats = (new Stats())
            ->setProfile($this->apiSession->profile->id)
            ->setColumn('questions_asked')
            ->run();

        $this->success($add);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $add = \App\Questions::where('id', $id)
            ->with('profile',
                'neighborhood',
                'category',
                'answers.recommendations',
                'answers.recommendations.profile',
                'answers.recommendations.profile.media',
                'media'
            )
            ->get();

        if (empty($add['0'])) $this->error('908');

        $this->success($add['0']);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $this->error('998');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, \App\Http\Requests\QuestionCreateRequest $request)
    {
        $fields = $request->rules();

        unset($this->input['image']);

        $use = array_intersect_key($this->input, $fields);

        $delete = \App\Questions::where('id', $id)->update($use);

        if (! $delete) $this->error('913');

        $this->success($delete);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $delete = \App\Questions::where('id', $id)->delete();

        if (! $delete) $this->error('912');

        $stats = (new Stats())
            ->setProfile($this->apiSession->profile->id)
            ->setColumn('questions_asked')
            ->setAddition('substract')
            ->run();

        $this->success($delete);
    }
}
