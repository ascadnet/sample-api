<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ListController extends Controller
{

    /**
     * Get a list of categories.
     */
    public function categories(\App\Http\Requests\ListsCategories $request)
    {
        $categories = (new \App\Categories())
            ->select('id', 'name', 'sub_category', 'status')
            ->where('status', '1');

        if (! empty($this->input['only_super_categories'])) {
            $categories->where('sub_category', '0');
        } else {
            if (! empty($this->input['super_category'])) {
                $categories->where('sub_category', $this->input['super_category']);
            } else {
                $categories->where('sub_category', '!=', '0');
            }
        }

        if (! empty($this->input['name']))
            $categories->where('name', 'like', '%' . $this->input['name'] . '%');

        // $list = $categories->orderBy('name')->get()->toArray();
        $list = $categories->get()->toArray();

        $aliases = (new \App\Categories())->getAliases();

        $full = array_merge($list, $aliases);

        // Cache it.
        $put = \Cache::forever($this->cacheKey, json_encode($full));

        $this->success($full);
    }


    /**
     * Get a list of neighborhoods.
     */
    public function neighborhoods(\App\Http\Requests\ListsNeighborhoods $request)
    {
        $neighborhoods = (new \App\GeoNeighborhoods())
            ->select('id', 'name', 'borough_id')
            ->where('metro_id', $this->input['city_id'])
            ->orderBy('name', 'asc');

        if (! empty($this->input['borough_id']))
            $neighborhoods->where('borough_id', $this->input['borough_id']);

        if (! empty($this->input['name']))
            $neighborhoods->where('name', 'like', '%' . $this->input['name'] . '%');

        $list = $neighborhoods->get();

        // Cache it.
        $put = \Cache::forever($this->cacheKey, json_encode($list));

        $this->success($list);
    }


    /**
     * Get boroughs
     */
    public function boroughs(\App\Http\Requests\ListsBoroughs $request)
    {
        $bo = \DB::connection('mysql')
            ->table('geo_metro_borough')
            ->select('geo_borough.id', 'geo_borough.name')
            ->join('geo_borough', 'geo_metro_borough.borough_id', '=', 'geo_borough.id')
            ->where('geo_metro_borough.metro_id', $this->input['city_id'])
            ->orderBy('name', 'asc');

        if (! empty($this->input['name']))
            $bo->where('geo_borough.name', 'like', '%' . $this->input['name'] . '%');

        $list = $bo->get();

        // Cache it.
        $put = \Cache::forever($this->cacheKey, json_encode($list));

        $this->success($list);
    }


}
