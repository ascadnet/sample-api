<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\lib\Stats;

class ShareController extends Controller
{

    /**
     *
     */
    public function question($id)
    {
        $this->after($id);

        $this->stats();

        $this->success();
    }

    /**
     *
     */
    public function profile($id)
    {
        $this->after($id);

        $this->stats();

        $this->success();
    }

    /**
     *
     */
    public function recommendation($id)
    {
        $this->after($id);

        $this->stats();

        $this->success();
    }


    private function stats()
    {
        $stats = (new Stats())
            ->setProfile($this->apiSession->profile->id)
            ->setColumn('shares')
            ->run();
    }

}
