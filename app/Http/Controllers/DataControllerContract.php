<?php namespace App\Http\Controllers;

/**
 * 
 * 
 * @author  j-belelieu
 * @date    6/30/15
 */

interface DataControllerContract {

    public function runCleanse();

}