<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class FlagController extends Controller
{

    protected $data = array();

    protected $type = '';


    /**
     * @param $id
     * @param Requests\FlagRequest $request
     */
    public function flagProfile($id, \App\Http\Requests\FlagRequest $request)
    {
        $this->type = 'profile';

        $item = \App\UserProfile::where('id', $id)->first();

        if (empty($item->id)) $this->error('910');

        $this->after($id, 'flag.profile');

        $this->completeFlag($item);
    }


    /**
     * @param $id
     * @param Requests\FlagRequest $request
     */
    public function flagRecommendation($id, \App\Http\Requests\FlagRequest $request)
    {
        $this->type = 'recommendation';

        $item = \App\Recommendations::where('id', $id)->first();

        if (empty($item->id)) $this->error('908');

        $this->after($id, 'flag.recommendation');

        $this->completeFlag($item);
    }


    /**
     * @param $id
     * @param Requests\FlagRequest $request
     */
    public function flagQuestion($id, \App\Http\Requests\FlagRequest $request)
    {
        $this->type = 'question';

        $item = \App\Questions::where('id', $id)->first();

        if (empty($item->id)) $this->error('909');

        $this->after($id, 'flag.question');

        $this->completeFlag($item);
    }


    /**
     * @param $item
     */
    protected function completeFlag($item)
    {
        $reason = (isset($this->input['reason'])) ? $this->input['reason'] : '';

        $flag = \App\Flags::create([
            'flagged_by' => $this->apiSession->profile->id,
            'item_flagged' => $item->id,
            'item_type' => $this->type,
            'reason' => $reason,
        ]);

        $this->data['type'] = $this->type;
        $this->data['reason'] = $reason;
        $this->data['item'] = $item->toArray();
        $this->data['by'] = $this->apiSession->profile->toArray();

        $mail = \Mail::send('emails.flag', $this->data, function ($m) {
            $m->subject('Flag: A ' . $this->type . ' has been flagged.')
                ->to('flag@romio.com')
                ->bcc('jonb@romio.com')
                ->from($this->apiSession->profile->email);
        });

        $this->success($flag);
    }

}
