<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\lib\Stats;

class TestController extends Controller
{


    public function stats()
    {

        $get = \DB::connection('mysql')
            ->table('user_profile')
            ->get();

        foreach ($get as $item) {

            $questions = \DB::connection('mysql')
                ->table('questions')
                ->where('posted_by', $item->id)
                ->count();

            $answers = \DB::connection('mysql')
                ->table('question_answers')
                ->where('answered_by', $item->id)
                ->count();

            $recommendations = \DB::connection('mysql')
                ->table('recommendations')
                ->where('posted_by', $item->id)
                ->count();

            $get = \DB::connection('mysql')
                ->table('user_stats')
                ->where('profile_id', $item->id);

            $find = $get->first();

            if (! empty($find->id)) {
                $up  = \DB::connection('mysql')
                    ->table('user_stats')
                    ->where('profile_id', $item->id)
                    ->update([
                        'questions_asked' => $questions,
                        'questions_answered' => $answers,
                        'recommendations' => $recommendations,
                    ]);

                echo "<li>Updating " . $item->id;
            } else {
                $up  = \DB::connection('mysql')
                    ->table('user_stats')
                    ->insert([
                        'profile_id' => $item->id,
                        'questions_asked' => $questions,
                        'questions_answered' => $answers,
                        'recommendations' => $recommendations,
                    ]);

                echo "<li>Creating " . $item->id;
            }

        }

        echo "A";
        exit;

    }

}
