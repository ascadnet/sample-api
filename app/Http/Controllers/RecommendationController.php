<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Queries\Query;
use App\Traits\ImageTraits;
use App\lib\Stats;

class RecommendationController extends Controller
{

    use ImageTraits;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $this->error('998');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->error('998');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @todo    Validate that question exists.
     * @todo    Remove profanities.
     *
     * @return Response
     */
    public function store(\App\Http\Requests\RecommendationCreateRequest $request)
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0');

        $question_id = '';

        if (! empty($this->input['question_id'])) {
            $master_question = \App\Questions::where('id', $this->input['question_id'])->get()->first();

            if (empty($master_question->id)) $this->error('905');

            $question_id = $master_question->id;
        }

        // If the service doesn't yet exist, we need to create a service
        // profile for the user and send a claim code.
        if (empty($this->input['recommends'])) {

            $cleanPhone = $this->cleanse->phone($this->input['phone']);

            $em = (! empty($this->input['email'])) ? $this->input['email'] : '';

            $profile = new \App\UserProfile();
            $findByPhone = $profile->findByPhoneOrEmail($cleanPhone, $em);
            if (! empty($findByPhone->id)) {
                $this->input['recommends'] = $findByPhone->id;
            }
            else {

                $claimCode = md5(time() . uniqid());

                $userData = [
                    'company' => (! empty($this->input['company'])) ? $this->input['company'] : '',
                    'first_name' => (! empty($this->input['first_name'])) ? $this->input['first_name'] : '',
                    'last_name' => (! empty($this->input['last_name'])) ? $this->input['last_name'] : '',
                    'email' => $this->input['email'],
                    'phone' => $cleanPhone,
                    'neighborhood_id' => (! empty($question_id)) ? $master_question->neighborhood_id : $this->input['neighborhood_id'],
                    'category_id' => (! empty($question_id)) ? $master_question->category_id : $this->input['category_id'],
                    'status' => '2',
                    'type' => 'service',
                    'password' => $claimCode,
                    'posted_by' => $this->apiSession->profile->id,
                ];

                $data = (new Query('User\CreateUser'))
                    ->run($userData)
                    ->getResult();

                if (empty($data['profile_id'])) $this->error('907');

                $this->input['recommends'] = $data['profile_id'];

                // Send the outreach email to marketing.
                $send = \Mail::send('emails.recommended_without_profile', $userData, function ($m) use($userData) {
                    $m->subject('Outreach: user has recommended someone without a profile.')
                        ->to('profiles@romio.com')
                        ->bcc('jonb@romio.com');

                    if (! empty($userData['email'])) {
                        $m->from($userData['email']);
                    } else {
                        $m->from('noaddress@romio.com');
                    }
                });

            }

        }

        // Now recommend them.
        if (! empty($question_id)) {
            $this->storeForQuestion($master_question);
        } else {
            $this->storeWithoutQuestion();
        }
    }


    /**
     * Recommend when answering a question.
     */
    public function storeForQuestion($master_question)
    {
        $master_profile = \App\UserProfile::where('id', $this->input['recommends'])->get()->first();

        if (empty($master_profile->id)) $this->error('906');

        // $this->input['super_category_id'] = $master_question->super_category_id;
        $this->input['category_id'] = $master_question->category_id;
        $this->input['neighborhood_id'] = $master_question->neighborhood_id;
        $this->input['status'] = '1';
        $this->input['posted_by'] = $this->apiSession->profile->id;


        $add = \App\Recommendations::create($this->input);

        $add1 = \App\QuestionAnswers::create([
            'status' => '1',
            'question_id' => $this->input['question_id'],
            'recommendation_id' => $add->id,
            'answered_by' => $this->apiSession->profile->id,
        ]);

        $this->storeImage($master_question->id, $this->input['recommends'], 'recommendation');

        $this->after($add1->id, 'answer.create');

        $stats = (new Stats())
            ->setProfile($this->apiSession->profile->id)
            ->setColumn('questions_answered')
            ->run();

        $stats = (new Stats())
            ->setProfile($this->apiSession->profile->id)
            ->setColumn('recommendations')
            ->run();

        $this->success($master_question);
    }


    /**
     * Just a plain recommendation, not answering a question.
     */
    public function storeWithoutQuestion()
    {
        //$master_category = (new \App\Categories)->getSuperCategory($this->input['category_id']);
        //$this->input['super_category_id'] = $master_category;

        $this->input['posted_by'] = $this->apiSession->profile->id;

        $add = \App\Recommendations::create($this->input);

        $this->storeImage($add->id, $this->input['recommends'], 'recommendation');

        $this->after($add->id, 'recommendation.create');

        $this->pushToFeed($add->id, 'recommendation');

        $stats = (new Stats())
            ->setProfile($this->apiSession->profile->id)
            ->setColumn('recommendations')
            ->run();

        $this->success($add);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $add = \App\Recommendations::where('id', $id)
            ->with(
                'profile',
                'profile.media',
                'neighborhood',
                'category',
                'media'
            )
            ->get();

        if (empty($add['0'])) $this->error('909');

        $this->success($add['0']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $this->error('998');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param   int     $id
     * @param
     *
     * @return Response
     */
    public function update($id, \App\Http\Requests\RecommendationUpdateRequest $request)
    {
        $fields = $request->rules();

        $use = array_intersect_key($this->input, $fields);

        $delete = \App\Recommendations::where('id', $id)->update($use);

        if (! $delete) $this->error('914');

        $this->success($delete);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $find = \App\QuestionAnswers::where('recommendation_id', $id)->get();

        if (! empty($find->id)) {
            $stats = (new Stats())
                ->setProfile($this->apiSession->profile->id)
                ->setColumn('questions_answered')
                ->setAddition('substract')
                ->run();
        }

        $delete = \App\Recommendations::where('id', $id)->delete();

        if (! $delete) $this->error('911');

        $stats = (new Stats())
            ->setProfile($this->apiSession->profile->id)
            ->setColumn('recommendations')
            ->setAddition('substract')
            ->run();

        $this->success($delete);
    }
}
