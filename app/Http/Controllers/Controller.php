<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request as Request;

use App\lib\FieldCleanse as FieldCleanse;
use App\lib\FieldFormat as FieldFormat;
use App\lib\Reply as Reply;
use App\lib\Points as Points;


abstract class Controller extends BaseController
{
    use DispatchesJobs, ValidatesRequests;

    protected $request;
    protected $input;
    protected $format;
    protected $taskName;
    protected $cleanse;
    protected $reply;
    protected $points;
    protected $apiSession;


    public function __construct(Request $request, FieldFormat $format, FieldCleanse $cleanse, Reply $reply, Points $points)
    {
        header('Content-Type: application/json');

        $this->request = $request;

        $this->input = $request->input();
        unset($this->input['key']);
        unset($this->input['session_id']);

        $this->format = $format;

        $this->reply = $reply;

        $this->cleanse = $cleanse;

        $this->taskName = $request->route()->getName();

        $this->cacheKey = $this->taskName . '-' . md5(serialize($this->input));

        $this->apiSession = app()->make('ApiSession');

        // For points
        // This would only not be present for web forms and non-api endpoints.
        if (! empty($this->apiSession->profile->id)) {
            $this->points = $points;
            $this->points->setProfile($this->apiSession->profile->id);
            $this->points->setTask($this->taskName);
        }

        $this->checkCache();
    }


    /**
     * @param   int         $id             The ID that was acted on.
     * @param   string      $force_type
     *
     * @return  int|mixed
     */
    public function after($id, $force_type = '')
    {
        $this->points->setActId($id);

        // These will be calculated automatically by the cron job.
        //->setCategory($category)
        //->setNeighborhood($neighborhood)
        //->setOwner($owner)

        if (! empty($force_type)) $this->points->setTask($force_type);

        return $this->points->run();
    }



    /**
     * Re-format fields according to site-wide rules.
     *
     * @return  mixed
     */
    public function cleanseAll()
    {
        // Clean the phone number.
        if (! empty($this->input['phone'])) {
            $this->input['phone'] = $this->cleanse->phone($this->input['phone']);
        }

        return $this->input;
    }


    /**
     * @param string $cache_key
     *
     * @return bool
     */
    public function checkCache($cache_key = '')
    {
        if (! empty($_GET['cache'])) { \Cache::forget($this->cacheKey); }

        if (! empty($cache_key)) $this->cacheKey = $cache_key;

        if (\Cache::has($this->cacheKey)) {
            if (! empty($_GET['debug'])) { echo "FROM CACHE<hr>"; }

            $value = \Cache::get($this->cacheKey);
            $this->success(json_decode($value), '104');
        }

        if (! empty($_GET['debug'])) { echo "NOT FROM CACHE<hr>"; }

        return false;
    }


    /**
     * Reply with error.
     *
     * @param string $code
     */
    public function error($code = '999')
    {
        $this->reply->setError(true);
        $this->reply->setCode($code);

        echo $this->reply;
        exit;
    }


    /**
     * Push item to feed.
     *
     * @param   int     $id
     * @param   string  $type
     *
     * @return
     */
    protected function pushToFeed($id, $type)
    {
        switch ($type) {
            case 'recommendation':
                $key_field = 'recc_id';
                break;
            case 'deal':
                $key_field = 'deal_id';
                break;
            case 'service':
                $key_field = 'serv_id';
                break;
            case 'question':
                $key_field = 'ques_id';
                break;
            default:
                return null;
        }

        return \App\Feed::create([
            'posted_by' => $this->apiSession->profile->id,
            'type' => $type,
            'session' => $this->apiSession->id,
            $key_field => $id,
        ]);
    }


    /**
     * Reply with success.
     *
     * @param   mixed   $data
     * @param   string  $code
     */
    public function success($data = '', $code = '100')
    {
        $this->reply->setCode($code);
        $this->reply->setData($data);

        echo $this->reply;
        exit;
    }


    /**
     * Add points to a profile
     *
     * @param   int       $item_id
     * @param   string    $item_type
     * @param   int       $item_owner
     * @param   int       $category_id
     * @param   int       $neighborhood_id
     *
     * @return  bool
     */
    public function addPoints($item_id, $item_type, $item_owner = 0, $category_id = 0, $neighborhood_id = 0)
    {
        // \App\ActivityTypes
        dd($this->taskName);

        $points = \App\ActivityTypes::where('route_ref', $this->taskName);
        if (empty($points->id)) return false;

        // Points for owner
        if (! empty($owner_id)) {

        }

        return true;
    }

}
