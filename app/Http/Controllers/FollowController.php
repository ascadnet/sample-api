<?php namespace App\Http\Controllers;

// use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\lib\Stats;

class FollowController extends Controller
{

    /**
     * Bulk of the follow functionality.
     *
     * @param   int     $id
     * @param   string  $type
     */
    protected function detectFollow($id, $type)
    {
        $code = 100;

        switch ($type) {
            case 'neighborhood':
                $key = 'neighborhood_id';
                $followType = 'follow.neighborhood';
                break;
            case 'category':
                $key = 'category_id';
                $followType = 'follow.category';
                break;
            case 'question':
                $key = 'question_id';
                $followType = 'follow.question';
                break;
            case 'borough':
                $key = 'borough_id';
                $followType = 'follow.borough';
                break;
            case 'city':
                $key = 'city_id';
                $followType = 'follow.city';
                break;
            default:
                $key = 'follow_id';
                $followType = 'follow.profile';
        }

        $findFollow = \App\UserFollows::where('follow_type', $type)
            ->where($key, $id)
            ->where('profile_id', $this->apiSession->profile->id);

        // Unfollowing...
        if (! empty($findFollow->id)) {
            $code = '103';

            $up = $findFollow->update([
                'status' => '0',
            ]);

            if ($followType == 'follow.profile') {
                $stats = (new Stats())
                    ->setProfile($this->apiSession->profile->id)
                    ->setColumn('users_followed')
                    ->setAddition('subtract')
                    ->run();

                $stats = (new Stats())
                    ->setProfile($id)
                    ->setColumn('users_following')
                    ->setAddition('subtract')
                    ->run();
            }

            $add = $findFollow->id;

            $this->after($add->id, 'un' . $followType);
        } else {
            $add = \App\UserFollows::create([
                'profile_id' => $this->apiSession->profile->id,
                'follow_type' => $type,
                $key => $id,
            ]);

            if ($followType == 'follow.profile') {
                $stats = (new Stats())
                    ->setProfile($this->apiSession->profile->id)
                    ->setColumn('users_followed')
                    ->run();

                $stats = (new Stats())
                    ->setProfile($id)
                    ->setColumn('users_following')
                    ->run();
            }

            $this->after($id, $followType);
        }

        $this->success($add, $code);
    }

    /**
     *
     */
    public function neighborhood($id)
    {
        $find = \App\GeoNeighborhoods::find($id);
        if (empty($find['id'])) $this->error('918');

        $this->detectFollow($id, 'neighborhood');
    }


    /**
     *
     */
    public function city($id)
    {
        $find = \App\GeoMetro::find($id);
        if (empty($find['id'])) $this->error('918');

        $this->detectFollow($id, 'borough');
    }


    /**
     *
     */
    public function borough($id)
    {
        $find = \App\GeoBorough::find($id);
        if (empty($find['id'])) $this->error('918');

        $this->detectFollow($id, 'borough');
    }


    /**
     *
     */
    public function profile($id)
    {
        if ($id == $this->apiSession->profile->id) $this->error('916');

        $find = \App\UserProfile::find($id);

        if (empty($find['id'])) $this->error('910');

        $this->detectFollow($id, 'profile');
    }

    /**
     *
     */
    public function category($id)
    {
        $find = \App\Categories::find($id);
        if (empty($find['id'])) $this->error('917');

        $this->detectFollow($id, 'category');
    }

    /**
     *
     */
    public function question($id)
    {
        $find = \App\Questions::find($id);
        if (empty($find['id'])) $this->error('909');

        $this->detectFollow($id, 'question');
    }

}
