<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;
use App\Queries\Query;

use App\UserProfile;
use App\UserSocial;
use App\UserDevices;

// use App\Http\Controllers\DataControllerContract;


class UserController extends Controller // implements DataControllerContract
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $this->error('998');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @route   GET /user
     *
     * @return Response
     */
    public function create()
    {
        $this->error('998');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @route   POST /user
     * @header  Accept  application/json
     *
     * @return  Response
     */
    public function store(\App\Http\Requests\UserStoreRequest $request)
    {
        $this->error('998');
        //$this->finalizeCreate($this->input);
    }



    /**
     * Create an account from a social website.
     *
     * Flow is as follows:
     *
     * 1. Find the social ID.
     *   1a. If found:
     *     a. Check for a new device.
     *       i. If found, log the user in.
     *       ii. If not found add the device and log the user in.
     *   1b. If not found:
     *     a. Try to find the user based on their email or phone.
     *       i. If found, add the new social site to the user's account, then log the user in.
     *       ii. If not found, create a new user.
     *
     * @param \App\Http\Requests\UserStoreRequest $request
     */
    public function socialCreate(\App\Http\Requests\UserSocialCreateRequest $request)
    {
        //\DB::statement('SET FOREIGN_KEY_CHECKS=0');

        $this->input = $this->cleanseAll($request->input());

        // Login or registration?
        $user_id = $this->findUserFromSocial($this->input['social_id'], $this->input['social_site']);

        if (! empty($user_id)) {
            $this->checkForNewDevice($user_id, $this->input['device_id'], $this->input['device_type']);
            $this->login($user_id);
        } else {
            // Login or registration?
            $user_id = $this->findUserFromEmail($this->input['email']);

            if (! empty($user_id)) {
                $this->addSocialSite($user_id, $this->input['social_id'], $this->input['social_site']);
            }
            else if (! empty($this->input['phone'])) {
                $user_id = $this->findUserFromPhone($this->input['phone']);

                if (! empty($user_id)) {
                    $this->addSocialSite($user_id, $this->input['social_id'], $this->input['social_site']);
                } else {
                    $this->finalizeCreate($this->input);
                }
            }
            else {
                $this->finalizeCreate($this->input);
            }
        }
    }


    /**
     * Confirm a session.
     */
    public function confirmSession($id)
    {
        $check = \App\Sessions::find($id);

        if (! empty($check->id) && $check->status == '1') {
            $this->success();
        } else {
            $this->error('904');
        }
    }


    /**
     * Check if the device the user is registering with/logging in with already
     * exists in the database, otherwise add it.
     *
     * @param   int     $user_id
     * @param   string  $device_id
     * @param   string  $device_type
     *
     * @return
     */
    protected function checkForNewDevice($user_id, $device_id, $device_type)
    {
        $find = UserDevices::select('user_id')
            ->where('device_id', $device_id)
            ->get()
            ->first();

        if (empty($find->user_id)) {
            $dv = (new UserDevices)->addDeviceToExistingUser($user_id, $device_id, $device_type);
        }

        return true;
    }


    /**
     * Create the user.
     *
     * Data comes from either store() or socialCreate() methods.
     *
     * @param   array   $input
     */
    protected function finalizeCreate(array $input)
    {
        $input = $this->fillCreateArray($this->input);

        $data = (new Query('User\CreateUser'))
            ->run($input)
            ->getResult();

        $this->login($data['user_id'], $data['profile_id']);
    }


    /**
     * @param   Int     $user_id
     * @param   String  $social_id
     * @param   String  $social_site
     */
    protected function addSocialSite($user_id, $social_id, $social_site)
    {
        $add = UserSocial::create([
            'user_id' => $user_id,
            'social_id' => $social_id,
            'social_site' => $social_site,
        ]);

        $this->apiSession->updateUser($user_id)
            ->updateProfile();

        if (empty($add->id)) {
            $this->error('902');
        } else {
            $this->success('', '101');
        }

        echo $this->reply;
        exit;
    }


    /**
     * Find a user based on a social ID.
     *
     * @param   string  $social_id
     * @param   string  $social_site
     *
     * @return  int
     */
    protected function findUserFromSocial($social_id, $social_site = '')
    {
        $find = UserSocial::select('user_id')
            ->where('social_id', $social_id)
            ->get()
            ->first();

        if (! empty($find->user_id)) return $find->user_id;

        return null;
    }


    /**
     *
     *
     * @param   string  $email
     *
     * @return  int
     */
    protected function findUserFromEmail($email)
    {
        $find = UserProfile::select('id', 'user_id')
            ->where('email', $email)
            ->get()
            ->first();

        if (! empty($find->user_id)) return $find->user_id;

        return null;
    }


    /**
     *
     *
     * @param   string  $phone
     *
     * @return  int
     */
    protected function findUserFromPhone($phone)
    {
        $find = UserProfile::select('id', 'user_id')
            ->where('phone', $phone)
            ->get()
            ->first();

        if (! empty($find->user_id)) return $find->user_id;

        return null;
    }


    /**
     * Fill/format data within the array being used to create the user.
     *
     * @param   array   $input
     *
     * @return  array
     */
    protected function fillCreateArray(array $input)
    {
        if (empty($input['status']))
            $input['status'] = '1';

        if (empty($input['password']))
            $input['password'] = Hash::make(md5(time() . $input['device_id']));

        return $input;
    }


    /**
     * Log a user into the app.
     *
     * @param   int     $user_id
     * @param   int     $profile_id
     */
    protected function login($user_id, $profile_id = '')
    {
        $this->apiSession->updateUser($user_id)
            ->updateProfile();

        $this->success('', '102');
    }


    /**
     * Get the primary profile ID for the user.
     *
     * @param   int     $user_id
     */
    protected function getPrimaryProfileId($user_id)
    {
        $find = UserProfile::where('user_id', $user_id)
            ->get()
            ->first();

        return $find;
    }


    /**
     * End a user's session within the app.
     */
    public function logout()
    {
        $this->apiSession->end();

        $this->reply->setCode('100');

        echo $this->reply;
        exit;
    }


    /**
     * Display the specified resource.
     *
     * @route   GET /user/{id}
     *
     * @param  int  $id
     * @return Response
    public function show($id)
    {

    }
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @route   GET /user/{id}
     *
     * @param  int  $id
     * @return Response
    public function edit($id)
    {

    }
     */

    /**
     * Update the specified resource in storage.
     *
     * @route   PUT /user/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $this->error('998');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @route   DELETE /user/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->error('998');
    }

}
