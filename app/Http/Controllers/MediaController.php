<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Traits\ImageTraits;

class MediaController extends Controller
{

    use ImageTraits;

    /**
     *
     */
    public function uploadProfilePicture()
    {
        if (! empty($this->input['profile_id'])) {
            $profile_id = $this->input['profile_id'];
        } else {
            $profile_id = $this->apiSession->profile->id;
        }

        $added = $this->storeImage($profile_id, $profile_id, 'profile_pic');

        $old = \App\UserMedia::where('id', '!=', $added)
            ->where('type', 'profile_pic')
            ->where('profile_id', $this->apiSession->profile->id)
            ->update([
                'type' => 'old_profile_pic',
            ]);

        $this->success($added);
    }


    /**
     *
     */
    public function uploadCoverPicture()
    {
        if (! empty($this->input['profile_id'])) {
            $profile_id = $this->input['profile_id'];
        } else {
            $profile_id = $this->apiSession->profile->id;
        }

        $added = $this->storeImage($profile_id, $profile_id, 'cover_pic');

        $old = \App\UserMedia::where('id', '!=', $added)
            ->where('type', 'cover_pic')
            ->where('profile_id', $this->apiSession->profile->id)
            ->update([
                'type' => 'old_cover_pic',
            ]);

        $this->success($added);
    }

}
