<?php namespace App\Http\Controllers;

use App\Http\Requests;

class ImportController extends Controller {


    protected function randomDate()
    {
        $time = time() - rand(0, 604800);
        return date('Y-m-d H:i:s', $time);
    }

    /**
     * importOldRecs
     */
    public function beginRecs()
    {
        set_time_limit(9999999);

        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        /*
        $get = \DB::connection('oldsql')
            ->table('question')
            ->take(10)
            ->get();

        foreach ($get as $item) {

            var_dump($item);

            $random = $this->randomDate();

            $check = (! empty($item->detail)) ? $item->detail : $item->question;

            $result = preg_match_all('/(?<=(in\s))(\s\w*)/', $check, $matches);

            $result = preg_split('/ in /', $check);
            if (count($result) > 1) {
                $neigh = preg_replace("/[^A-Za-z0-9 ]/", '', $result[1]);

                echo $result[1] . '---' . $neigh;
                echo "<HR>";
            }

            $makeQuestion = [
                'created_at' => $random,
                'updated_at' => $random,
                'status' => 1,
                'posted_by' => $item->post_as,
                'category_id' => $this->categoryConvert($item->category_id),
                'neighborhood_id' => '',
                'question' => $check,
            ];

            $answers = \DB::connection('oldsql')
                ->table('answer')
                ->where('question_id', $item->id)
                ->get();

            foreach ($answers as $ans) {



            }

        }
        exit;
        */


        $get = \DB::connection('oldsql')
            ->table('recommendation_post')
            ->select('recommendation_post.*') // , 'recommendation_post.id as mainId', 'category_item.user_id', 'category_item.category_id', 'recommendation_network_details.neighborhood_id')
            //->join('recommendation_network_details', 'recommendation_post.id', '=', 'recommendation_network_details.item_id')
            //->join('category_item', 'category_item.item_id', '=', 'recommendation_post.recommendation_id')
            //->take(100)
            ->orderBy('updated_at', 'desc')
            //->where('recommendation_post.id', 43)
            ->get();

        foreach ($get as $item) {

            $get1 = \DB::connection('oldsql')
                ->table('recommendation_network_details')
                ->where('recommendation_network_details.item_id', $item->id)
                ->first();

            $get2 = \DB::connection('oldsql')
                ->table('category_item')
                ->where('category_item.item_id', $item->id)
                ->first();

            //echo "<HR>";var_dump($get1, $get2, $item);

            $ne = (! empty($get1->neighborhood_id)) ? $get1->neighborhood_id : 193907;

            $ca = (! empty($get2->category_id)) ? $get2->category_id : 4015;

            /*
            $answer = \DB::connection('oldsql')
                ->table('answer_recommend')
                ->join('answer', 'answer.id', '=', 'answer_recommend.answer_id')
                ->where('recommendation_id', $item->recommendation_id)
                ->first();
            */

            $profile = \DB::connection('mysql')
                ->table('user_profile')
                ->where('id', $item->recommendation_id)
                ->first();

            if (! empty($profile->id) && ! empty($ca)) {

                var_dump($item);

                $rand = $this->randomDate();

                $adding = [
                    'id' => $item->id,
                    'created_at' => $rand,
                    'updated_at' => $rand,
                    'recommends' => $profile->id,
                    'posted_by' => $item->post_as,
                    'reason' => $item->text,
                    'category_id' => $this->categoryConvert($ca),
                    'neighborhood_id' => $this->neighborhoodConvert($ne),
                ];

                var_dump($adding); echo "<HR>";

                $add = \DB::connection('mysql')->table('recommendations')->insert($adding);

            }
        }

        echo "<HR><HR><HR>DONE!";
        exit;

    }


    /**
     * importOld
     */
    public function begin()
    {
        $up = 0;

        $clean = new \App\lib\FieldCleanse;

        set_time_limit(9999999);

        /*
        $get = \DB::connection('oldsql')
            ->table('romio')
            //->where('id', 1993)
            //->take(100)
            ->get();

        foreach ($get as $item) {

            var_dump($item);

            $getProfile = \DB::connection('oldsql')
                ->table('profile_info')
                ->where('user_id', $item->id)
                ->get();

            // Create the user
            $insertedUserData = [
                'id' => $item->id,
                'password' => 'required',
                'status' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];

            $add = \DB::connection('mysql')
                ->table('users')
                ->insert($insertedUserData);

            $insertedUserId = $item->id;

            foreach ($getProfile as $aProfile) {

                $up++;

                //var_dump($aProfile);

                if (empty($aProfile->metro_id)) {
                    $metro = 220;
                } else {
                    $metro = $aProfile->metro_id;
                }

                if ($aProfile->biz_type == 1) {
                    $type = 'service';
                } else {
                    $type = 'user';
                }

                $neigh_id = '';
                if (! empty($aProfile->neighborhood_id)) {
                    $neigh_id = $this->neighborhoodConvert($aProfile->neighborhood_id);
                }

                $cate_id = '';
                if (! empty($aProfile->category_id)) {
                    $cate_id = $this->categoryConvert($aProfile->category_id);
                }

                $insertProfileData = [
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'id' => $aProfile->id,
                    'user_id' => $insertedUserId,
                    'type' => $type,
                    'country_id' => '1',
                    'city_id' => '220',
                    'neighborhood_id' => $neigh_id,
                    'category_id' => $cate_id,
                    'company' => $aProfile->company,
                    'email' => $aProfile->email,
                    'first_name' => $aProfile->first_name,
                    'last_name' => $aProfile->last_name,
                    'headline' => $aProfile->short_service_headline,
                    'description' => $aProfile->service_headline,
                    'website' => $aProfile->website,
                    'phone' => $clean->phone($aProfile->phone),
                    'gender' => ($aProfile->gender == 1) ? 'female' : 'male',
                ];

                $addp = \DB::connection('mysql')
                    ->table('user_profile')
                    ->insert($insertProfileData);

                $insertProfileId = $aProfile->id;

                $privacy = \DB::connection('mysql')
                    ->table('user_privacy')
                    ->insert([
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                        'profile_id' => $insertProfileId,
                        'email' => $aProfile->show_email,
                        'phone' => $aProfile->show_phone,
                        'first_name' => 1,
                        'last_name' => 1,
                    ]);

                $uStats = \DB::connection('mysql')
                    ->table('user_stats')
                    ->insert([
                        'profile_id' => $insertProfileId,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]);

                // $aProfile->expert_category
                // $aProfile->expert_neighborhood
                if ($aProfile->is_expert == 1) {
                    $neigh1 = '';
                    if (! empty($aProfile->expert_neighborhood)) {
                        $neigh1 = $this->neighborhoodConvertName($aProfile->expert_neighborhood);
                    }

                    $exp = \DB::connection('mysql')
                        ->table('user_experts')
                        ->insert([
                            'profile_id' => $insertProfileId,
                            'neighborhood_id' => $neigh1,
                            'category_id' => '',
                            'city_id' => 220,
                            'badge_name' => $aProfile->expert_category,
                        ]);
                }

                if (! empty($aProfile->facebook_id)) {
                     $addFb = \DB::connection('mysql')
                        ->table('user_social')
                        ->insert([
                            'user_id' => $insertedUserId,
                            'social_site' => 'facebook',
                            'social_id' => $aProfile->facebook_id,
                        ]);
                }

                $scored = \DB::connection('oldsql')
                    ->table('user_score_details')
                    ->where('user_id', $item->id)
                    ->get();

                foreach ($scored as $userScore) {
                    $add = \DB::connection('mysql')
                        ->table('user_scores')
                        ->insert([
                            'profile_id' => $insertProfileId,
                            'neighborhood_id' => $this->neighborhoodConvert($userScore->neighborhood_id),
                            'category_id' => $this->categoryConvert($userScore->category_id),
                            'score' => $userScore->score,
                        ]);
                }
            }

            echo "<HR>";

        }
        */


        $upa = 0;

        $get = \DB::connection('oldsql')
            ->table('profile_info')
            ->whereNull('user_id')
            //->take(100)
            ->get();

        foreach ($get as $item) {

            $upa++;

            $userId = \DB::connection('mysql')
                ->table('users')
                ->insertGetId([
                    'password' => 'required',
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);

            if ($item->biz_type == 1) {
                $type = 'service';
            } else {
                $type = 'user';
            }

            $neigh_id = '';
            if (! empty($item->neighborhood_id)) {
                $neigh_id = $this->neighborhoodConvert($item->neighborhood_id);
            }

            $cate_id = '';
            if (! empty($item->category_id)) {
                $cate_id = $this->categoryConvert($item->category_id);
            }

            $insertProfileData = [
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'id' => $item->id,
                'user_id' => $userId,
                'type' => $type,
                'country_id' => '1',
                'city_id' => '220',
                'neighborhood_id' => $neigh_id,
                'category_id' => $cate_id,
                'company' => $item->company,
                'email' => $item->email,
                'first_name' => $item->first_name,
                'last_name' => $item->last_name,
                'headline' => $item->short_service_headline,
                'description' => $item->service_headline,
                'website' => $item->website,
                'phone' => $clean->phone($item->phone),
                'gender' => ($item->gender == 1) ? 'female' : 'male',
            ];

            $addp = \DB::connection('mysql')
                ->table('user_profile')
                ->insert($insertProfileData);

            $uStats = \DB::connection('mysql')
                ->table('user_stats')
                ->insert([
                    'profile_id' => $item->id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
        }



        echo "<HR><HR>DONE ($up // $upa)!";
        exit;
    }


    protected function neighborhoodConvert($id)
    {
        if (empty($id)) return 0;

        if ($id == 193907) return 193907;

        $neigh = \DB::connection('oldsql')
            ->table('neighborhoods')
            ->where('id', $id)
            ->first();

        if (empty($neigh->name)) return 0;

        $neigh_new = \DB::connection('mysql')
            ->table('geo_neighborhoods')
            ->where('name', $neigh->name)
            ->first();

        if (! empty($neigh_new->id)) return $neigh_new->id;

        return 0;
    }


    protected function neighborhoodConvertName($name)
    {
        $name = trim(str_replace('neighborhood', '', strtolower($name)));

        if ($name == 'nyc') $name = 'new york city';

        $cate_new = \DB::connection('mysql')
            ->table('geo_neighborhoods')
            ->where('name', $name)
            ->first();

        if (! empty($cate_new->id)) return $cate_new->id;

        return 0;
    }


    protected function categoryConvert($id)
    {
        if (empty($id)) return 0;

        if ($id == 4015) return 4015;

        $cate = \DB::connection('oldsql')
            ->table('category')
            ->where('id', $id)
            ->first();

        if (! empty($cate->name)) {
            $cate_new = \DB::connection('mysql')
                ->table('categories')
                ->where('name', $cate->name)
                ->first();

            if (empty($cate_new->id)) return 0;

            return $cate_new->id;
        } else {
            return 0;
        }
    }




    /**
     *
     */
    public function expertCategory()
    {
        $csv = array_map('str_getcsv', file('/Applications/MAMP/htdocs/romio/_docs/categories/list1.csv'));

        $topline = array_shift($csv);

        foreach ($csv as $item) {
            $find = \DB::connection('mysql')
                ->table('categories')
                ->where('name', $item['1'])
                ->first();

            if (! empty($find->id)) {

                if (! empty($item['3'])) {
                    $insert = \DB::connection('mysql')
                        ->table('categories_groups')
                        ->insert([
                            'main_category_id' => $find->id,
                            'name'             => $item['3'],
                        ]);
                } else {
                    echo "<LI>" . $item['1'] . ' (No Name)';
                }

            } else {
                echo "<li>" . $item['1'];
            }
        }

        dd($topline, $csv);
    }
}
