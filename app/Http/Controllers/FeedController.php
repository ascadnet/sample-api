<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\lib\Activity;


class FeedController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(\App\Http\Requests\FeedRequest $request)
    {

        // Rules:
        // - Friends always appear above experts.
        // - Neighborhood is the base criteria. This can be expanded to city-wide.
        // - Display neighboring hoods (Maponics API)
        // - Many services mark all of NYC, so we need to include that in the search for any neighborhood.
        //
        // Includes:
        // - Questions (answers, unanswered, etc.).
        // - Recommendations
        // - New services
        // - Deals

        /**
         * @link    http://docs.romioapi.com/?c=feed&p=index.md
         *
         * No Filters:
         *
         * 1. Get all experts in a user's area.
         *
         * 2. Get all items from people or places this user is following.
         *    - Set up methods to get activity by:
         *      GEO: neighborhood | borough | city
         *      PROFILE: profile | expert | service
         *
         * 3. Filters basically limit which methods we call.
         *    -> neighborhoods (array)
         *    -> category
         *
         *    - filter_type
         *      - all: $types = ['questions', 'recommendations', 'deals']
         *      - questions: $types = ['questions']
         *      - recommendations: $types = ['recommendations']
         *
         *    - filter_who:
         *      - all
         *      - friends
         *      - experts
         *
         *    METHODS:
         *    "Activity" Model
         *    - activity($types, $neighborhoods, $category)
         *          -> friends()
         *          -> follow()
         *          -> experts()
         *          -> neighborhoods()
         *          -> category()
         */

        $activity = new Activity();

        $activity->setProfile($this->apiSession->profile->id);

        if (! empty($this->input['take'])) $activity->setDisplay($this->input['take']);

        if (! empty($this->input['page'])) $activity->setPage($this->input['page']);


        if (! empty($this->input['category'])) $activity->setCategories($this->input['category']);

        if (! empty($this->input['neighborhood'])) $activity->setNeighborhoods([$this->input['neighborhood']]);

        if (! empty($this->input['filter_who'])) $activity->setWho($this->input['filter_who']);

        // if (! empty($this->input['filter_type'])) $activity->set([$this->input['filter_type']]);

        $final = $activity->run()->get();

        $this->success($final);
        exit;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->error('998');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $this->error('998');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $this->error('998');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $this->error('998');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $this->error('998');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->error('998');
    }
}
