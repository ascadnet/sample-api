<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Traits as Traits;


class ServiceController extends Controller
{

    use Traits\ImageTraits;
    use Traits\UserTraits;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $this->error('998');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->error('998');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(\App\Http\Requests\ServiceCreateRequest $request)
    {
        $this->input['type'] = 'service';

        $this->input = $this->cleanseAll($this->input);

        $profile = $this->createProfile($this->apiSession->user_id, $this->input);

        if (empty($profile)) $this->error('907');

        $files = $this->request->file();

        foreach ($files as $key => $aFile) {
            if ($key == 'cover_image') { $type = 'cover_pic'; }
            else if ($key == 'image') { $type = 'profile_pic'; }
            else { $type = 'other'; }

            $store = $this->storeImage('', $profile, $type, $key);
        }

        $this->pushToFeed($profile, 'service');

        $this->success($profile);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $profile = new \App\UserProfile;

        $query = $profile->where('id', $id)
            ->with(
                'neighborhood',
                'category',
                'privacy',
                'follows',
                //'scores',
                'experts',
                'experts.category',
                'experts.neighborhood',
                'stats'
            );

        $get = $query->get();

        if (empty($get['0'])) {
            $this->error('910');
        } else {
            $use = $get['0'];
            $user_id = $use->user_id;
        }

        $test = \App\UserProfile::find($id);

        $recommended = $test->recommended()
            ->orderBy('created_at', 'desc')
            ->take(5)
            ->with('owner', 'category', 'neighborhood')
            ->get()
            ->toArray();

        $recommendations = $test->recommendations()
            ->with('profile', 'category', 'neighborhood')
            ->with(['profile.media' => function ($query) {
                $query->where('type', 'profile_pic');
                $query->orWhere('type', 'cover_pic');
            }])
            ->orderBy('created_at', 'desc')
            ->take(5)
            ->get()
            ->toArray();

        $questions = $test->questions()
            /*
            ->with('profile')
            ->with(['profile.media' => function ($query) {
                $query->where('type', 'profile_pic');
                $query->orWhere('type', 'cover_pic');
            }])
            */
            ->with('category', 'neighborhood')
            ->orderBy('created_at', 'desc')
            ->take(5)
            ->get()
            ->toArray();

        $activity = [
            'questions' => $questions,
            'recommended' => $recommended,
            'recommendations' => $recommendations,
        ];

        $use->activity = $activity;

        $use->services = $test->whereHas('services', function ($query) {
            $query->where('user_profile.user_id', '=', $this->apiSession->user_id);
            $query->where('user_profile.type', '=', 'service');
        })->with('category', 'neighborhood')->get();

        $use->media = \DB::table('user_media')
            ->where('profile_id', $id)
            ->where(function($test) {
                $test->where('type', 'profile_pic')
                    ->orWhere('type', 'cover_pic');
            })
            ->get();

        $this->success($use);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $this->error('998');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, \App\Http\Requests\ServiceCreateRequest $request)
    {
        $this->input = $this->cleanseAll($this->input);

        $pid = \App\UserProfile::where('id', $id)
            // ->where('type', 'service')
            ->first();

        if (empty($pid->id)) $this->error('910');

        $update = $pid->update($this->input);

        $this->success($update);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $delete = \App\UserProfile::where('id', $id)
            ->delete();

        if (! $delete) $this->error('915');

        $this->success($delete);
    }


}
