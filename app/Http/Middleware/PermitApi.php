<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\lib\Reply;
use App\lib\ApiSession;

class PermitApi
{

    protected $reply;

    /**
     * @param Reply $reply
     */
    public function __construct(Reply $reply)
    {
        $this->reply = $reply;
    }

    /**
     * Handle an incoming request and confirm that this user is
     * permitted to use the API.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $key = $request->input('key');
        $name = $request->route()->getName();

        \Log::info('Request: ', $request->input());
        \Log::info('Server: ', $_SERVER);
        \Log::info('URI: ' . $_SERVER['REQUEST_URI']);
        \Log::info('---------------------------------------------');

        if (empty($key) || $key != \Config::get('api.key')) {

            $this->reply->setError(true);
            $this->reply->setCode('901');
            echo $this->reply;
            exit;

        } else {

            $sesId = trim($request->input('session_id'));

            // Confirm Session ID is present unless we are logging in or registering.
            if (
                $name != 'user.socialCreate' &&
                $name != 'user.confirmSession' &&
                empty($sesId)
            ) {
                $this->reply->setError(true);
                $this->reply->setCode('900');
                echo $this->reply;
                exit;
            }

            // Proceed
            $ses = app()->make('ApiSession');

            if (! empty($sesId)) {
                $sesData = $ses->setId($sesId)->get();

                if ($ses->error) {

                    if (! empty($_GET['debug'])) dd($ses);

                    $this->reply->setError(true);
                    $this->reply->setCode('904');

                    echo $this->reply;
                    exit;
                } else {
                    $ses->update();
                    $this->activity($sesData, $request);
                }

            } else {
                $ses->create();
            }

        }

        return $next($request);
    }



    public function activity($sesData, $request)
    {
        $route = $request->route();

        $add = \App\Activity::create([
            'session_id' => $sesData->id,
            'type' => $route->getName(),
            'parameters' => serialize($route->parameters()),
        ]);
    }

}
