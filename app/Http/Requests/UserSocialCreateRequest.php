<?php namespace App\Http\Requests;

use App\Http\Requests\ResponseTrait;
use App\Http\Requests\Request;
use App\Traits\UserTraits;

class UserSocialCreateRequest extends Request
{

    use ResponseTrait;
    use UserTraits;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $universalCreateRules = $this->getUniveralUserCreateRules();

        $add = [
            'city' => '',
            'state' => '',
            'country' => '',
            'picture' => 'url',
            'cover_picture' => 'url',
        ];

        return array_merge($universalCreateRules, $add);
    }

}
