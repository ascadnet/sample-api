<?php namespace App\Http\Requests;

use App\Http\Requests\ResponseTrait;
use App\Http\Requests\Request;

class ServiceCreateRequest extends Request
{

    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => 'integer|required_with:company',
            'neighborhood_id' => 'integer|required',

            'company' => '',
            'headline' => '',
            'description' => '',
            'website' => 'url',
            'phone' => 'required_without:email',
            'email' => 'required_without:phone',

            'image' => 'image',
            'cover_image' => 'image',
        ];
    }

}
