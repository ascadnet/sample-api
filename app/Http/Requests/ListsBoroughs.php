<?php namespace App\Http\Requests;

use App\Http\Requests\ResponseTrait;
use App\Http\Requests\Request;

class ListsBoroughs extends Request
{

    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city_id' => 'integer|required',
            'name' => 'min:2|alpha_dash',
        ];
    }

}
