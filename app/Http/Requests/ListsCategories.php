<?php namespace App\Http\Requests;

use App\Http\Requests\ResponseTrait;
use App\Http\Requests\Request;

class ListsCategories extends Request
{

    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'only_super_categories' => 'integer',
            'super_category' => 'integer',
            'name' => 'min:2',
        ];
    }

}
