<?php namespace App\Http\Requests;

use App\Http\Requests\ResponseTrait;
use App\Http\Requests\Request;

class RecommendationCreateRequest extends Request
{

    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'question_id' => 'integer',

            'reason' => '',
            'recommends' => 'integer|required_without_all:last_name,company',

            'first_name' => 'required_without_all:recommends,company',
            'last_name' => 'required_without_all:recommends,company',

            'category_id' => 'required_without:question_id',
            'neighborhood_id' => 'required_without:question_id',

            'phone' => 'required_without_all:recommends,email',
            'email' => 'required_without_all:recommends,phone',

            'image' => 'image',

            'company' => 'required_without_all:recommends,first_name,last_name',
            'website' => '',
        ];
    }

}
