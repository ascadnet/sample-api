<?php namespace App\Http\Requests;

use App\Http\Requests\ResponseTrait;
use App\Http\Requests\Request;

class FeedRequest extends Request
{

    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category' => 'array',
            'neighborhood' => 'array',
            'filter_who' => 'in:all,friends,experts',
            'filter_type' => 'in:all,questions,recommendations',
            'page' => 'integer',
            'take' => 'integer',
        ];
    }

}
