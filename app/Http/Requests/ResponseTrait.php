<?php namespace App\Http\Requests;

use Illuminate\Http\JsonResponse;
use App\lib\Reply;

trait ResponseTrait {

    /**
     * Overwrites the standard response() method found
     * in the Illuminate\Foundation\Http\FormRequest class.
     */
    public function response(array $errors)
    {
        $reply = new Reply;
        $reply->setError(true);
        $reply->setCode('902');
        $reply->setData($errors);

        echo $reply;
        exit;
    }

}