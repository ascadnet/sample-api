<?php

/**
 * Resourceful Controllers
 *
 * Create the controller:
 *  - php artisan make:controller PhotoController
 *
 * Create the route:
 *  - Route::resource('name', 'NameController');
 *
 * GET	        /xxx	        index	    controller.index
 * GET	        /xxx/create	    create	    controller.create
 * POST	        /xxx	        store	    controller.store
 * GET	        /xxx/{id}	    show	    controller.show
 * GET	        /xxx/{id}/edit	edit	    controller.edit
 * PUT/PATCH	/xxx/{id}	    update	    controller.update
 * DELETE	    /xxx/{id}	    destroy	    controller.destroy
 */


Route::group(['middleware' => 'permit', 'prefix' => '/v2'], function () {


    // -------------------------------
    // Lists

    \Route::get('lists/categories', [
        'as' => 'lists.categories',
        'uses' => 'ListController@categories',
    ]);

    \Route::get('lists/neighborhoods', [
        'as' => 'lists.neighborhoods',
        'uses' => 'ListController@neighborhoods',
    ]);

    \Route::get('lists/boroughs', [
        'as' => 'lists.boroughs',
        'uses' => 'ListController@boroughs',
    ]);


    // -------------------------------
    // Flagging

    \Route::post('flag/profile/{id}', [
        'as' => 'flag.profile',
        'uses' => 'FlagController@flagProfile'
    ]);

    \Route::post('flag/question/{id}', [
        'as' => 'flag.question',
        'uses' => 'FlagController@flagQuestion'
    ]);

    \Route::post('flag/recommendation/{id}', [
        'as' => 'flag.recommendation',
        'uses' => 'FlagController@flagRecommendation'
    ]);


    // -------------------------------
    // User Functionality

    \Route::post('user/expert/apply', [
        'as' => 'user.expert.apply',
        'uses' => 'UserController@applyForExpert',
    ]);
    \Route::post('user/logout', [
        'as' => 'user.logout',
        'uses' => 'UserController@logout',
    ]);
    \Route::get('user/confirmSession/{id}', [
        'as' => 'user.confirmSession',
        'uses' => 'UserController@confirmSession',
    ]);
    \Route::post('user/social', [
        'as' => 'user.socialCreate',
        'uses' => 'UserController@socialCreate',
    ]);

    \Route::resource('/user', 'UserController');


    // -------------------------------
    // Media Functionality

    \Route::post('media/profile_pic', [
        'as' => 'media.profile',
        'uses' => 'MediaController@uploadProfilePicture',
    ]);

    \Route::post('media/cover_pic', [
        'as' => 'media.cover',
        'uses' => 'MediaController@uploadCoverPicture',
    ]);


    // -------------------------------
    // Share

    \Route::post('share/profile/{id}', [
        'as' => 'share.profile',
        'uses' => 'ShareController@profile',
    ]);

    \Route::post('share/question/{id}', [
        'as' => 'share.question',
        'uses' => 'ShareController@question',
    ]);

    \Route::post('share/recommendation/{id}', [
        'as' => 'share.recommendation',
        'uses' => 'ShareController@recommendation',
    ]);


    // -------------------------------
    // Follow

    \Route::post('follow/profile/{id}', [
        'as' => 'follow.profile',
        'uses' => 'FollowController@profile',
    ]);

    \Route::post('follow/neighborhood/{id}', [
        'as' => 'follow.neighborhood',
        'uses' => 'FollowController@neighborhood',
    ]);

    \Route::post('follow/category/{id}', [
        'as' => 'follow.category',
        'uses' => 'FollowController@category',
    ]);

    \Route::post('follow/question/{id}', [
        'as' => 'follow.question',
        'uses' => 'FollowController@question',
    ]);


    // -------------------------------
    // Feed

    \Route::resource('/feed', 'FeedController');


    // -------------------------------
    // Service/Profile

    \Route::resource('/service', 'ServiceController');


    // -------------------------------
    // Recommendations/Answers

    \Route::resource('/recommendation', 'RecommendationController');


    // -------------------------------
    // Questions

    \Route::resource('/question', 'QuestionController');





    // --------------------------------------------------------------
    // Future Development

    \Route::resource('/deal', 'DealController');

    \Route::resource('/notification', 'NotificationController');

    \Route::resource('/search', 'SearchController');

    // \Route::resource('/message', 'MessageController');

});




// --------------------------------------------------------------
// Temp web form submission tool.

\Route::post('webSubmit', [
    'as' => 'web.betaInterestForm',
    'uses' => 'WebController@betaInterestForm',
]);


\Route::post('test/stats', [
    'uses' => 'TestController@stats',
]);


/*
\Route::get('importOld', [
    'uses' => 'ImportController@begin',
]);

\Route::get('importOldRecs', [
    'uses' => 'ImportController@beginRecs',
]);

\Route::get('importExpertCategories', [
    'uses' => 'ImportController@expertCategory',
]);
*/


\Route::get('/', function() {
    header('Location: http://www.romio.com/');
    exit;
});
