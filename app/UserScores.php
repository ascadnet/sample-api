<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Holds the current score for a profile within a category and neighborhood.
 * Calculated by cron from data within the user_score_log table.
 *
 * @date    2015-06-19
 * @author  j-belelieu
 */
class UserScores extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_scores';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'profile_id',
        'neighborhood_id',
        'category_id',
        'score',
        'change',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


    public function profile()
    {
        return $this->belongsTo('\App\UserProfile', 'id', 'profile_id');
    }

}
