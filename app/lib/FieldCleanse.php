<?php

namespace App\lib;

/**
 * 
 * 
 * @author  j-belelieu
 * @date    6/19/15
 */

class FieldCleanse {

    /**
     * Clean phone numbers.
     * Remove +1 from start and remove any non-numeric number.
     *
     * @param   string  $input
     *
     * @return  string
     */
    public function phone($input)
    {
        if (substr($input, 0, 2) == '+1') $input = substr($input, 2);

        if (strlen($input) > 10 && substr($input, 0, 1) == '1') $input = substr($input, 1);

        return preg_replace("/[^a-zA-Z0-9]/", '', $input);
    }


    /**
     * @param   string  $input
     */
    public function name($input)
    {

    }


    /**
     * Clean profanity.
     *
     * @param   string  $input
     */
    public function profanity($input)
    {

    }

}