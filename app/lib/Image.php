<?php namespace App\lib;

/**
 * 
 * 
 * @author  j-belelieu
 * @date    6/23/15
 */

// use App\lib\ImageResize;

use App\UserMedia as UserMedia;


class Image {


    protected $resizer;

    protected $media;

    protected $type = 'profile_pic';

    protected $media_type = 'image';

    protected $profileId;

    /**
     * @var This is either the request file object (for uploads) or a URL
     *           for non-uploads. Example request object:
     *              $this->request->file('image')
     */
    protected $path;

    protected $source;

    protected $actId;

    protected $mediaId;

    protected $act_key_field;

    protected $error = false;


    /**
     *
     */
    public function __construct()
    {
        // $this->resizer = new ImageResize();
        $this->media = new UserMedia();
    }

    /**
     * @return  int
     */
    public function getMediaId()
    {
        return $this->mediaId;
    }

    /**
     * @param   int  $id
     */
    public function setProfile($id)
    {
        $this->profileId = $id;

        return $this;
    }

    /**
     * @param   int  $id
     */
    public function setActId($id)
    {
        $this->actId = $id;

        return $this;
    }


    /**
     * @param   int  $source
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * @param   string  $path
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @param   string  $type
     */
    public function setType($type)
    {
        $lower = strtolower($type);

        switch ($lower) {
            case 'profile_pic':
            case 'cover_pic':
                $this->type = $lower;
                break;
            case 'question':
                $this->act_key_field = 'question_id';
                $this->type = $lower;
                break;
            case 'recommendation':
                $this->act_key_field = 'recommendation_id';
                $this->type = $lower;
                break;
            case 'service':
                $this->act_key_field = 'service_id';
                $this->type = $lower;
                break;
            default:
                $this->type = 'other';
        }

        return $this;
    }

    /**
     * @param   string  $type
     */
    public function setMediaType($type)
    {
        $lower = strtolower($type);

        switch ($lower) {
            case 'image':
            case 'video':
                $this->media_type = $lower;
                break;
            default:
                $this->media_type = 'other';
        }

        return $this;
    }

    /**
     *
     */
    public function run()
    {
        if (empty($this->profileId) && empty($this->actId)) return false;

        // Download from URL, for example from Facebook.
        if (is_string($this->path)) {
            try {
                $expA = explode('?', $this->path);

                $fb = false;

                // https://graph.facebook.com/1149159580/picture
                if (strpos($expA['0'], 'graph.') !== false) {
                    $fileGet = get_headers($expA['0']);
                    foreach ($fileGet as $line) {
                        if (strpos($line, 'Location: ') !== false) {
                            $fb = true;
                            $this->path = str_replace('Location: ', '', $line);

                            $image_contents = @file_get_contents($this->path);

                            if ($image_contents) {
                                $filen['0'] = md5(time() . rand(0,9999)) . '.jpg';
                                $filen['1'] = 'jpg';

                                $target = public_path() . '/images/' . $filen['0'];

                                $save = @file_put_contents($target, $image_contents);
                            }

                            break;
                        }
                    }
                }

                if (! $fb) {
                    $filen = $this->makeName($expA['0']);

                    \Log::info('Image A', $filen);

                    $target = public_path() . '/images/' . $filen['0'];

                    $image_contents = @file_get_contents($this->path);

                    if ($image_contents) {
                        $save = @file_put_contents($target, $image_contents);
                    }
                }

            } catch (Exception $e) {
                $this->error = true;

                $save = $this->path;
            }
        }
        // Download from uploaded file
        else {
            $filen = $this->makeName($this->path->getClientOriginalName());

            \Log::info('Image B', $filen);

            $target = public_path() . '/images/';

            $this->path->move($target, $filen['0']);

            $save = true;
        }

        if ($save) {
            $media1 = $this->media->create([
                'profile_id' => $this->profileId,
                'source' => $this->source,
                'raw_image_path' => $filen['0'],
                'format' => $filen['1'],
                'type' => $this->type,
                'media_type' => $this->media_type,
                // 'act_id' => $this->actId,
                $this->act_key_field => $this->actId,
            ]);

            $this->mediaId = $media1->id;

            return $media1->id;
        } else {
            return null;
        }
    }


    /**
     * @param $inputName
     *
     * @return array
     */
    protected function makeName($inputName)
    {
        $expB = explode('.', $inputName);

        $ext = strtolower($expB[sizeof($expB)-1]);

        switch ($this->type) {
            case 'profile_pic':
                return [
                    'profile-' . $this->profileId . '-' . time() . '.' . $ext,
                    $ext
                ];
            case 'cover_pic':
                return [
                    'cover-' . $this->profileId . '-' . time() . '.' . $ext,
                    $ext
                ];
            default:
                return [
                    md5(time() . uniqid()) . '.' . $ext,
                    $ext
                ];
        }
    }

}