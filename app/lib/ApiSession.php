<?php namespace App\lib;

use App\Sessions;
use App\UserDevices;
use App\UserProfile;

/**
 *
 * 
 * @author  j-belelieu
 * @date    6/24/15
 */
class ApiSession {


    public $id;
    public $user_id;
    public $profile;
    public $last_activity;

    public $error = false;
    public $errorSpot = '';

    private $model;
    private $data;
    private $profile_id;
    private $device;


    /**
     * @param string $id
     */
    public function __construct()
    {
        $this->model = new Sessions;
    }


    /**
     * @return string
     */
    public function __toString()
    {
        return json_encode($this);
    }


    /**
     * Set the session ID received from the API.
     *
     * @param   string  $id
     *
     * @return  $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }


    /**
     * Set the user ID for this session.
     *
     * @param   int     $id
     *
     * @return  $this
     */
    public function setUserId($id)
    {
        $this->user_id = $id;

        return $this;
    }


    /**
     * Set the profile ID.
     *
     * @param   Int     $profile
     *
     * @return  $this
     */
    public function setProfileId($profile)
    {
        $this->profile_id = $profile;

        return $this;
    }


    /**
     * Set the device ID.
     *
     * @param   Object     $did
     *
     * @return  $this
     */
    public function setDevice($did)
    {
        $this->device = $did;

        return $this;
    }


    /**
     * Get a session.
     */
    public function get()
    {
        if (! empty($this->id)) {
            $this->data = $this->model->where('id', $this->id)->get()->first();

            if (! empty($this->data->id)) {

                $this->last_activity = $this->data->updated_at;

                if ($this->data->status == 1) {

                    $this->profile = UserProfile::where('id', $this->data->profile_id)
                        ->with('media')
                        ->get()
                        ->first();

                    $this->user_id = $this->data->user_id;

                    $this->update();

                } else {
                    $this->errorSpot = 'A2';
                    $this->end();
                }

            } else {
                $this->errorSpot = 'A3';
                $this->end();
            }
        } else {
            $this->error = true;
            $this->errorSpot = 'A1';
        }

        return $this;
    }


    /**
     * Create a new session. This is run the first time the
     * user logs in.
     */
    public function create()
    {
        $id = $this->generateId();

        $request = app()->make('request');

        $this->model->create([
            'id' => $id,
            'device_id' => $request->input('device_id'),
        ]);

        $this->setId($id);
    }


    /**
     * Change the profile associated with the session.
     * user vs service vs expert
     */
    public function updateProfile()
    {
        $profile = UserProfile::where('user_id', $this->user_id)
            ->get()
            ->first();

        $this->model->where('id', $this->id)->update([
            'profile_id' => $profile->id,
            'profile_type' => $profile->type,
        ]);

        return $this;
    }


    /**
     * Only called at login.
     *
     * @param   Int     $user_id
     */
    public function updateUser($user_id)
    {
        $this->model->where('id', $this->id)->update([
            'user_id' => $user_id,
        ]);

        $this->user_id = $user_id;

        return $this;
    }


    /**
     *
     */
    public function end()
    {
        $this->model->where('id', $this->id)->update([
            'status' => 0,
            'ended_at' => date('Y-m-d H:i:s'),
        ]);

        $this->error = true;
    }


    /**
     * Update an existing session.
     */
    public function update()
    {
        $this->model->where('id', $this->id)->update([
            'status' => 1,
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }


    /**
     * Generate an ID.
     *
     * @return  string  $id
     */
    protected function generateId()
    {
        return md5('vjFE8G*d9QKFlanzp' . time()) . uniqid();
    }

}