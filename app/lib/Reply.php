<?php namespace App\lib;

use Illuminate\Http\Request as Request;

/**
 * Generates a reply from the API to the client.
 *
 * All replies to the app should go through this!
 * 
 * @author  j-belelieu
 * @date    6/19/15
 */

class Reply {

    /**
     * @var bool
     */
    public $error = false;

    /**
     * @var string  Status code.
     */
    public $code = '100';

    /**
     * @var string  Status update.
     */
    public $message = '';

    /**
     * @var Object  Payload of data.
     */
    public $data;

    /**
     * @var Object  Session data.
     */
    public $session;

    /**
     * @var Object  List of features and status.
     */
    public $appStatus;

    public $requestTime;

    private $format = 'json';


    /**
     *
     */
    public function __construct()
    {
        header('Content-Type: application/json');
    }


    /**
     * @return string
     */
    public function __toString()
    {
        $this->session = app()->make('ApiSession')->get();

        $this->requestTime = microtime(true) - BOOTSTRAP_START_TIME;

        return json_encode($this);
    }


    /**
     * @param   string  $format
     */
    public function setFormat($format)
    {
        switch ($format) {
            case 'json':
                $this->format = $format;
                break;
        }
    }

    /**
     * 100 = OK
     * 401 = Incorrect API key.
     *
     * @param   integer  $code
     */
    public function setCode($code)
    {
        $this->code = $code;

        $message = \Config::get('codes.' . $code);
        $this->setMessage($message);
    }


    /**
     * @param   bool    $error
     */
    public function setError($error)
    {
        $this->error = $error;
    }


    /**
     * @param   string  $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }


    /**
     * @param   mixed
     */
    public function setData($data)
    {
        $this->data = $data;
    }


}