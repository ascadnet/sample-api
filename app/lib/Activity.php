<?php namespace App\lib;

/**
 * Builds an activity list based on:
 * 1. The profile viewing.
 * 2. Filters: neighborhoods, category, who (all, friends, experts)
 * 3.
 * 
 * @author  j-belelieu
 * @date    7/20/15
 */

class Activity {

    private $profile;

    private $neighborhoods = [];

    private $services = [];

    private $categories = [];

    private $type = 'all';

    private $who = 'all';

    private $page = 1;

    private $display = 10;

    private $skip = 0;

    // Default to NYC
    private $city = 220;

    // -----
    protected $followers;


    protected $final_profile_ids = [];

    protected $results;

    protected $questions;

    protected $recommendations;


    /**
     * @param mixed $profile
     *
     * @return  $this
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * @param int $page
     *
     * @return  $this
     */
    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * @param int $display
     *
     * @return  $this
     */
    public function setDisplay($display)
    {
        $this->display = $display;

        return $this;
    }

    /**
     * @param string $type
     *
     * @return  $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @param array $neighborhoods
     *
     * @return  $this
     */
    public function setNeighborhoods($neighborhoods)
    {
        $this->neighborhoods = $neighborhoods;

        return $this;
    }

    /**
     * @param int $city
     *
     * @return  $this
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @param array $categories
     *
     * @return  $this
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;

        $this->categories();

        return $this;
    }

    /**
     * @param string $who
     *
     * @return  $this
     */
    public function setWho($who)
    {
        $this->who = strtolower($who);

        return $this;
    }


    /**
     * Get the feed results.
     *
     * @return  mixed
     */
    public function get()
    {
        return $this->results;
    }


    /**
     * Run and build the feed.
     */
    public function run()
    {
        $this->neighborhoods();

        // Add yourself to output
        if ($this->who == 'all') $this->final_profile_ids[] = $this->profile;

        // Skip
        $this->skip = $this->page * $this->display - $this->display;

        // Who is being displayed?
        switch ($this->who) {
            case 'experts':
                $this->experts();
                break;
            case 'friends':
                $this->friends();
                break;
            default:
                $this->experts();
                $this->friends();
        }

        switch ($this->type) {
            case 'questions':
                $this->questions();
                break;
            case 'recommendations':
                $this->recommendations();
                break;
            case 'deals':
                $this->deals();
                break;
            case 'services':
                $this->services();
                break;
            default:
                $this->questions();
                $this->recommendations();
                $this->deals();
                //$this->services();
        }

        $quesArray = $this->questions->toArray();

        foreach ($quesArray as &$entry) {
            $entry['_type'] = 'question';
        }

        $recArray = $this->recommendations->toArray();

        foreach ($recArray as &$entry) {
            $entry['_type'] = 'recommendation';
        }

        $serviceArray = [];
        if ($this->services) {
            $serviceArray = $this->services->toArray();

            foreach ($serviceArray as &$entry) {
                $entry['_type'] = 'service';
            }
        }

        $sorted = array_merge($quesArray, $recArray, $serviceArray);

        usort($sorted, [$this, 'date_compare']);

        $this->results = array_reverse($sorted);

        return $this;
    }

    /**
     * @param $a
     * @param $b
     *
     * @return int
     */
    private function date_compare($a, $b)
    {
        $t1 = strtotime($a['created_at']);
        $t2 = strtotime($b['created_at']);
        return $t1 - $t2;
    }

    /**
     * Questions
     */
    private function questions()
    {
        $questions = (new \App\Questions)
            ->with('profile',
                'profile.media',
                'media',
                'neighborhood',
                'category',
                'answers',
                'answers.profile',
                'answers.profile.media',
                'answers.recommendations',
                'answers.recommendations.profile',
                'answers.recommendations.profile.media'
            )
            ->whereIn('posted_by', $this->final_profile_ids);

        if (! empty($this->categories)) {
            $questions->whereIn('category_id', $this->categories);
        }

        if (! empty($this->neighborhoods)) {
            if (is_array($this->neighborhoods['0'])) $this->neighborhoods = $this->neighborhoods['0'];

            $questions->whereIn('neighborhood_id', $this->neighborhoods);
        }

        $questions->orderBy('created_at', 'desc')->skip($this->skip)->take($this->display);

        $this->questions = $questions->get();
    }

    /**
     *
     */
    private function recommendations()
    {
        $recs = (new \App\Recommendations)
            ->with('profile',
                'profile.media',
                'owner',
                'owner.media',
                'media',
                'neighborhood',
                'category'
            )
            ->whereIn('posted_by', $this->final_profile_ids);

        if (! empty($this->categories)) {
            $recs->whereIn('category_id', $this->categories);
        }

        if (! empty($this->neighborhoods)) {
            $recs->whereIn('neighborhood_id', $this->neighborhoods);
        }

        $recs->orderBy('created_at', 'desc')->skip($this->skip)->take($this->display);

        $this->recommendations = $recs->get();
    }

    /**
     *
     */
    private function deals()
    {

    }

    /**
     *
     */
    private function services()
    {
        $services = (new \App\UserProfile)
            ->with('media',
                'neighborhood',
                'category',
                'experts',
                'stats'
            )
            ->where('type', 'service')
            ->whereIn('id', $this->final_profile_ids);

        if (! empty($this->categories)) {
            $services->whereIn('category_id', $this->categories);
        }

        if (! empty($this->neighborhoods)) {
            $services->whereIn('neighborhood_id', $this->neighborhoods);
        }

        $services->orderBy('created_at', 'desc')->skip($this->skip)->take($this->display);

        $this->services = $services->get();
    }


    /**
     * Followers are included in this. You automatically follow all of your friends!
     */
    private function friends()
    {
        $follow = \DB::table('user_follows')
            ->select('follow_id')
            ->where('follow_type', 'profile')
            ->where('profile_id', $this->profile)
            ->get();

        $elem = [];

        foreach ($follow as $item) {
            if ($item->follow_id) $elem[] = $item->follow_id;
        }

        $this->combineProfiles($elem);
    }


    /**
     * Experts
     */
    private function experts()
    {
        $question = \DB::table('user_experts')
            ->select('profile_id')
            ->whereIn('neighborhood_id', $this->neighborhoods);

        if (! empty($this->categories)) {
            $question->whereIn('category_id', $this->categories, 'or');
        }

        $get = $question->get();

        $elem = [];

        foreach ($get as $item) {
            if ($item->profile_id) $elem[] = $item->profile_id;
        }

        $this->combineProfiles($elem);
    }

    /**
     * Builds a list of unique profile IDs that will provide content
     * for this itteration of the feed.
     *
     * @param   array   $new
     */
    private function combineProfiles(array $new)
    {
        $this->final_profile_ids = array_unique(array_merge($this->final_profile_ids, $new));
    }

    /**
     * Applicable neighborhoods
     */
    private function neighborhoods()
    {
        if (empty($this->neighborhoods)) {
            $geo = new \App\GeoNeighborhoods;
            $get = $geo->inCity($this->city);

            $this->neighborhoods = [];
            foreach ($get as $flight) {
                $this->neighborhoods[] = $flight->id;
            }
        }
    }

    /**
     * Applicable categories.
     */
    private function categories()
    {
        if (! empty($this->categories)) {

            $final = [];
            foreach ($this->categories as $item) {
                if ($item <= 4015) {
                    $cats = new \App\Categories;
                    $get = $cats->select('id')->where('sub_category', $item)->get();

                    foreach ($get as $innerItem) {
                        $final[] = $innerItem->id;
                    }

                    $this->categories = $final;
                } else {
                    $final[] = $item;
                }
            }

        }
    }


}