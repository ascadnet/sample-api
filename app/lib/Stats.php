<?php namespace App\lib;

/**
 * 
 * 
 * @author  j-belelieu
 * @date    7/13/15
 */

class Stats {

    private $addition = 'add';
    private $column;
    private $profile;


    /**
     *
     */
    public function __construct()
    {

    }


    /**
     * @param string $addSubtract
     *
     * @return  $this
     */
    public function setAddition($addSubtract)
    {
        $this->addition = $addSubtract;

        return $this;
    }


    /**
     * @param mixed $profile
     *
     * @return  $this
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;

        return $this;
    }


    /**
     * @param mixed $column
     *
     * @return  $this
     */
    public function setColumn($column)
    {
        $this->column = $column;

        return $this;
    }


    /**
     *
     */
    public function run()
    {
        if (empty($this->profile) || empty($this->column)) return false;

        if ($this->addition == 'subtract') {
            return $this->subtract();
        } else {
            return $this->add();
        }
    }


    /**
     *
     */
    public function add()
    {
        return \DB::connection('mysql')
            ->table('user_stats')
            ->where('profile_id', $this->profile)
            ->increment($this->column);
    }


    /**
     *
     */
    public function subtract()
    {
        return \DB::connection('mysql')
            ->table('user_stats')
            ->where('profile_id', $this->profile)
            ->decrement($this->column);
    }


}