<?php namespace App\lib;

/**
 * 
 * 
 * @author  j-belelieu
 * @date    7/13/15
 */

class Points {

    private $profile;
    private $category;
    private $neighborhood;
    private $owner;
    private $actId;
    private $task;
    private $pointsOwner = 0;
    private $pointsUser = 0;



    /**
     * @param mixed $actId
     *
     * @return  $this
     */
    public function setActId($actId)
    {
        $this->actId = $actId;

        return $this;
    }


    /**
     * @param mixed $profile
     *
     * @return  $this
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;

        return $this;
    }


    /**
     * @param mixed $category
     *
     * @return  $this
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @param mixed $neighborhood
     *
     * @return  $this
     */
    public function setNeighborhood($neighborhood)
    {
        $this->neighborhood = $neighborhood;

        return $this;
    }

    /**
     * @param mixed $owner
     *
     * @return  $this
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @param mixed $task
     *
     * @return  $this
     */
    public function setTask($task)
    {
        $points = \App\ActivityTypes::select('id', 'score_for_user', 'score_for_owner')
            ->where('route_ref', $task)
            ->first();

        if (! empty($points->id)) {
            $this->setPointsOwner($points->score_for_owner)->setPointsUser($points->score_for_user);
            $this->task = $task;
        }

        return $this;
    }

    /**
     * @param int $pointsUser
     *
     * @return  $this
     */
    public function setPointsUser($pointsUser)
    {
        $this->pointsUser = $pointsUser;

        return $this;
    }

    /**
     * @param int $pointsOwner
     *
     * @return  $this
     */
    public function setPointsOwner($pointsOwner)
    {
        $this->pointsOwner = $pointsOwner;

        return $this;
    }


    /**
     *
     */
    public function run()
    {
        if (empty($this->task)) return false;

        return (new \App\UserScoreLog())->insert([
            'type' => $this->task,
            'act_id' => $this->actId,
            'profile_id' => $this->profile,
        ]);

        /*
        if (! empty($this->owner) && $this->pointsOwner > 0) {
            $add = new \App\UserScoreLog();

            $combine = [
                'profile_id' => $this->owner,
                'points' => $this->pointsOwner,
            ];

            dd(array_merge($combine, $base));

            $id = $add->insert(array_merge($combine, $base));

            dd('here owner', $id);
        }

        if (! empty($this->profile) && $this->pointsUser > 0) {
            $add = new \App\UserScoreLog();

            $combine = [
                'profile_id' => $this->profile,
                'points' => $this->pointsUser,
            ];

            $id = $add->insert(array_merge($combine, $base));

            dd('here user', $id);
        }
        */
    }

}