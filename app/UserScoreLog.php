<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Log of all activity that will affect the user's score. Used to calculate scores
 * that will be stored in the user_scores table.
 *
 * @date    2015-06-19
 * @author  j-belelieu
 */
class UserScoreLog extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_score_log';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'profile_id',
        'neighborhood_id',
        'category_id',
        'voted_by',
        'type',
        'points',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


    public function profile()
    {
        return $this->belongsTo('\App\UserProfile', 'id', 'profile_id');
    }

}
