<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 *
 * @date    2015-06-19
 * @author  j-belelieu
 */
class Sessions extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sessions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'ended_at',
        'user_id',
        'profile_id',
        'device_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

}
