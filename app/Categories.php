<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sub_category',
        'name',
        'status',
        'default_background',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var     array
     */
    protected $hidden = [];


    /**
     * Get the master category ID for a sub-category.
     *
     * @param   int     $id
     *
     * @return  int
     */
    public function getSuperCategory($id)
    {
        $master_category = \App\Categories::where('id', $id)->get()->first();

        if (! empty($master_category->sub_category)) return $master_category->sub_category;

        return $id;
    }


    /**
     *
     */
    public function getAliases()
    {
        $get = \DB::table('category_aliases')
            ->select(
                'category_aliases.category_id as id',
                'category_aliases.alias as name',
                'category_aliases.status',
                'categories.sub_category'
            )
            ->join('categories', 'categories.id', '=', 'category_aliases.category_id');
            //->orderBy('name');

        return $get->get();
    }

}
