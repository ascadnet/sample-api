<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryAliases extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'category_aliases';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id',
        'alias',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var     array
     */
    protected $hidden = [];


}
