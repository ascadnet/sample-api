<?php namespace App;

use App\lib\ApiSession;
use Illuminate\Database\Eloquent\Model;

/**
 * Primary profile table for users.
 *
 * @param   user_id     ID of the user this profile belongs to.
 * @param   type        1 = User / 2 = Service / 3 = Unclaimed
 *
 * @date    2015-06-19
 * @author  j-belelieu
 */
class UserProfile extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_profile';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'type',
        'country_id',
        'city_id',
        'neighborhood_id',
        'category_id',
        'email',
        'first_name',
        'last_name',
        'company',
        'headline',
        'description',
        'website',
        'phone',
        'gender',
    ];


    public function user()
    {
        return $this->belongsTo('\App\Users', 'id', 'user_id');
    }

    public function privacy()
    {
        return $this->hasOne('\App\UserPrivacy', 'profile_id', 'id');
    }

    public function mediaAll()
    {
        return $this->hasMany('\App\UserMedia', 'profile_id', 'id');
    }

    public function media()
    {
        return $this->mediaAll()->where('type', 'cover_pic')->orWhere('type', 'profile_pic');
    }

    public function services()
    {
        return $this->hasMany('\App\UserProfile', 'user_id', 'user_id');
        /*
        return \DB::table('user_profile')
            ->where('type', 'service')
            ->where('user_id', $user_id)
            ->get();
        */
    }

    public function follows()
    {
        return $this->hasMany('\App\UserFollows', 'profile_id', 'id');
    }

    public function questions()
    {
        return $this->hasMany('\App\Questions', 'posted_by', 'id');
    }

    public function recommendations()
    {
        return $this->hasMany('\App\Recommendations', 'posted_by', 'id');
    }

    public function recommended()
    {
        return $this->hasMany('\App\Recommendations', 'recommends', 'id');
    }

    public function scoreLog()
    {
        return $this->hasMany('\App\UserScoreLog', 'profile_id', 'id');
    }

    public function scores()
    {
        return $this->hasMany('\App\UserScores', 'profile_id', 'id');
    }

    public function experts()
    {
        return $this->hasMany('\App\UserExperts', 'profile_id', 'id');
    }

    public function neighborhood()
    {
        return $this->hasOne('\App\GeoNeighborhoods', 'id', 'neighborhood_id');
    }

    public function category()
    {
        return $this->hasOne('\App\Categories', 'id', 'category_id');
    }

    public function stats()
    {
        return $this->hasOne('\App\UserStats', 'profile_id', 'id');
    }


    /**
     * Find a profile using the email, phone number, or both.
     *
     * @param   int     $phone
     * @param   string  $email
     *
     * @return  bool
     */
    public function findByPhoneOrEmail($phone = '', $email = '')
    {
        $query = \DB::table($this->table);

        if (! empty($phone) && ! empty($email)) {
            $query->where('phone', $phone);
            $query->orWhere('email', $email);
        }
        else if (! empty($phone)) {
            $query->where('phone', $phone);
        }
        else if (! empty($email)) {
            $query->where('phone', $email);
        }
        else {
            return false;
        }

        return $query->first();
    }

}
