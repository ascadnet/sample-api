<?php

return [

    '100' => 'OK',
    '101' => 'Added new social media account to existing user.',
    '102' => 'Logged user in from existing social media connection.',
    '103' => 'Unfollowed.',

    '900' => 'The session_id parameter is required.',
    '901' => 'Invalid API key.',
    '902' => 'Validation error.',
    '903' => 'Error creating user.',
    '904' => 'Session has expired or is invalid.',
    '905' => 'Error creating recommendations: question does not exist.',
    '906' => 'The profile you are recommending does not exist.',
    '907' => 'Could not create service profile.',
    '908' => 'Could not find recommendation.',
    '909' => 'Could not find question.',
    '910' => 'Could not find profile.',
    '911' => 'Could not delete recommendation. Are you sure it exists?',
    '912' => 'Could not delete question. Are you sure it exists?',
    '913' => 'Could not update question. Are you sure it exists?',
    '914' => 'Could not update recommendation. Are you sure it exists?',
    '915' => 'Could not delete service. Are you sure it exists?',
    '916' => 'You cannot follow yourself.',
    '917' => 'Could not find category.',
    '918' => 'Could not find neighborhood.',

    '997' => 'Route does not exist.',
    '998' => 'The endpoint you have requested exists but is not implemented.',
    '999' => 'No code specified, but an error occurred.',

];