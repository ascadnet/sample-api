<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        /**
         *
         */
        Schema::create('sessions', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->timestamps();

            $table->integer('user_id');
            $table->integer('profile_id');
            $table->string('key');

            // 1 = Active
            // 0 = Inactive
            $table->tinyInteger('status')->default('1');

            $table->index('user_id');
            $table->index('profile_id');
            $table->index('key');
        });

        Schema::create('activity', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->timestamps();

            $table->integer('user_id');
            $table->integer('profile_id');
            $table->integer('neighborhood_id'); // Where activity took place.
            $table->integer('category_id');
            $table->tinyInteger('type');

            $table->index('user_id');
            $table->index('profile_id');
            $table->index('neighborhood_id');
            $table->index('category_id');
        });

        Schema::create('activity_types', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->string('description');
            $table->integer('score_for_owner'); // Points awarded to the user who owns the item.
            $table->integer('score_for_user'); // POints awarded to the user who performed the act.
        });

        // Insert is done via RAW SQL. See file
        DB::insert("
          INSERT INTO activity_types (id, description, score_for_owner, score_for_user) VALUES
            (1,'user added a question',2,0),
            (2,'user added a answer',5,0),
            (3,'user voted up an answer',0,2),
            (4,'user followed a neighborhood',1,0),
            (5,'user followed another user',1,5),
            (6,'user followed city',0,0),
            (7,'someone following user',0,0),
            (8,'user followed a recommendation',0,0),
            (9,'user added a recommendation',5,0),
            (10,'bulk recommendation',0,0),
            (11,'user added a request',2,0),
            (12,'marked as expert',0,0),
            (13,'user down voted an answer',0,-2),
            (14,'borough follow',0,0),
            (15,'user announced a service',0,0),
            (16,'user up voted a recommendation',0,2),
            (17,'user up voted a question',0,2),
            (18,'user up voted a request',0,2),
            (19,'user added a deal',0,0),
            (20,'user followed a question',1,1),
            (21,'user followed a category',1,0),
            (22,'marked as best answer',0,25),
            (23,'invitation sent',2,0),
            (24,'invitation accepted',0,5),
            (25,'share a recommendation globally',2,0),
            (26,'share a recommendation',2,2),
            (27,'share a question request globally',2,0),
            (28,'share a question request',2,2),
            (29,'share a answer globally',2,0),
            (30,'share a answer',2,2),
            (35,'user added tags',1,0),
            (36,'fb friends joined romio',0,0),
            (37,'added friends list',0,0),
            (38,'user down voted a recommendation',0,-2),
            (39,'user flagged a recommendation',0,-5),
            (40,'user added a answer in 5 mins',25,0),
            (41,'most upvotes for an recommendation',0,25),
            (42,'most upvotes for an answer',0,25),
            (43,'first time recommendation',5,0),
            (44,'user cautioned a business',1,0),
            (45,'User recommendation downvoted',0,-2),
            (48,'Verified Expert added a recommendation',25,0),
            (49,'Verified Expert added a question',10,0),
            (50,'Verified Expert added a answer',25,0),
            (51,'Verified Expert',5000,0)
        ");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sessions');
        Schema::drop('activity');
        Schema::drop('activity_types');
    }
}
