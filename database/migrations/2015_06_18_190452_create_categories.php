<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Full list of categories.
         */
        Schema::create('categories', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->timestamps();

            $table->integer('sub_category');
            $table->string('name');

            // 1 = active
            // 0 = inactive
            $table->tinyInteger('status');

            // 1 = person (example: "Cross fit intructors")
            // 0 = not person (example: "Cross fit")
            $table->tinyInteger('is_person');

            $table->index('sub_category');
        });

        /**
         * Category names and aliases
         */
        Schema::create('category_aliases', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('category_id');

            $table->string('alias');

            $table->index('category_id');
        });

        /**
         * List of user-suggested categories.
         */
        Schema::create('categories_suggested', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->timestamps();

            $table->string('name');
            $table->integer('sub_category');

            // 1 = pending
            // 0 = not approved
            // 2 = rejected
            $table->tinyInteger('status');
            $table->string('status_reason');

            $table->index('sub_category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories');
        Schema::drop('category_aliases');
        Schema::drop('categories_suggested');
    }
}
