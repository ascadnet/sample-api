<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsAnswers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         *
         */
        Schema::create('questions', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->timestamps();

            // 0 = Deleted
            // 1 = Live
            // 2 = Flagged
            $table->tinyInteger('status');

            $table->integer('posted_by'); // Profile ID
            $table->integer('category_id');
            $table->integer('neighborhood_id');
            $table->string('question');

            $table->index('posted_by');
            $table->index('category_id');
            $table->index('neighborhood_id');
        });

        /**
         *
         */
        Schema::create('question_answers', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->timestamps();

            // 0 = Deleted
            // 1 = Live
            // 2 = Flagged
            $table->tinyInteger('status');

            $table->integer('question_id');
            $table->integer('answered_by');
            $table->integer('category_id');
            $table->integer('neighborhood_id');
            $table->string('answer');
            $table->integer('recommends'); // Profile ID

            $table->index('question_id');
            $table->index('answered_by');
            $table->index('category_id');
            $table->index('recommends');
            $table->index('neighborhood_id');
        });

        /**
         *
         */
        Schema::create('question_answer_scores', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->dateTime('updated_at');

            $table->integer('answer_id');
            $table->integer('score');

            $table->index('answer_id');
        });

        /**
         *
         */
        Schema::create('question_answer_score_log', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->dateTime('created_at');

            $table->integer('answer_id');
            $table->integer('profile_id');

            // 1 = Up
            // 2 = Down
            // 3 = Share
            $table->tinyInteger('type');

            $table->index('answer_id');
            $table->index('profile_id');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('questions');
        Schema::drop('question_answers');
        Schema::drop('question_answer_scores');
        Schema::drop('question_answer_score_log');
    }
}
