<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * List of available deals.
         */
        Schema::create('deals', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->timestamps();

            $table->integer('offered_by'); // Profile ID

            // 1 = Active
            // 2 = Fully Claimed
            // 0 = Removed/Deleted
            $table->tinyInteger('status')->default('1');

            // 1 = Free
            // 2 = Paid
            $table->tinyInteger('type')->default('1');

            $table->dateTime('starts');
            $table->dateTime('ends');

            $table->integer('total_offered');
            $table->integer('total_claimed');

            // Use these to calculate user savings!
            $table->decimal('base_cost', 8, 2); // Regular price = $100
            $table->decimal('user_cost', 8, 2); // User price = $30.

            $table->string('name');
            $table->mediumText('description');

            $table->index('offered_by');
        });


        /**
         * List of users who have claimed a deal.
         */
        Schema::create('deals_claimed', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->timestamps();

            $table->integer('deal_id');
            $table->integer('claimed_by');

            $table->string('transaction_number');

            $table->index('deal_id');
            $table->index('claimed_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('deals');
        Schema::drop('deals_claimed');
    }
}
