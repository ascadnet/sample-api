<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Basic users table.
         */
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->timestamps();

            $table->string('password', 60);

            // 1 = Active
            // 0 = Suspended
            // 2 = Unclaimed
            $table->tinyInteger('status')->default('1');

            $table->rememberToken();
        });

        /**
         * Profile details for a user or service.
         */
        Schema::create('user_profile', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->timestamps();

            $table->integer('user_id');

            // 1 = User
            // 2 = Service
            // 3 = Unclaimed
            $table->tinyInteger('type')->default('1');

            //$table->integer('total_score');
            $table->integer('country_id')->default('1'); // USA
            $table->integer('city_id')->default('220'); // New York City
            $table->integer('neighborhood_id');

            $table->string('email');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('company')->nullable();
            $table->string('headline')->nullable();
            $table->string('description')->nullable();
            $table->string('website')->nullable();
            $table->string('phone')->nullable();

            $table->index('user_id');
            $table->index('email');
            $table->index('city_id');
            $table->index('neighborhood_id');
        });

        /**
         * Images associated with a user or service.
         */
        Schema::create('user_images', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->timestamps();

            $table->integer('profile_id');

            $table->string('path');
            $table->enum('format', ['jpg', 'png']);

            // 1 = Primary
            // 2 = Secondary
            $table->tinyInteger('type')->default('1');

            $table->index('profile_id');
        });

        /**
         * Privacy concerns: what to display on profiles.
         */
        Schema::create('user_privacy', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->timestamps();

            $table->integer('profile_id');

            // 0 = Nobody
            // 1 = Everyone
            // 2 = Followers
            $table->tinyInteger('email')->default('1');
            $table->tinyInteger('first_name')->default('1');
            $table->tinyInteger('last_name')->default('1');
            $table->tinyInteger('phone')->default('1');

            $table->index('profile_id');
        });

        /**
         * Social media connections for an account.
         */
        Schema::create('user_social', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->timestamps();

            $table->integer('user_id');
            $table->string('social_id');

            // 1 = Facebook
            // 2 = Twitter
            // 3 = LinkedIn
            // 4 = Google
            $table->tinyInteger('site');

            $table->index('user_id');
            $table->index('social_id');
        });


        /**
         * List of devices associated with the user.
         */
        Schema::create('user_devices', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->timestamps();

            $table->integer('user_id');
            $table->string('device_id');

            // 1 = iOS
            // 2 = Android
            $table->tinyInteger('type')->default('1');

            $table->index('user_id');
            $table->index('device_id');
        });

        /**
         * User score log: calculated values. Updated via cron based on
         * data within the user_score_log table.
         */
        Schema::create('user_scores', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->dateTime('updated_at');

            $table->integer('profile_id');
            $table->integer('neighborhood_id');
            $table->integer('category_id');
            $table->integer('score');
            $table->integer('change');

            $table->index('profile_id');
            $table->index('neighborhood_id');
            $table->index('category_id');
        });

        /**
         * User score log. This table is used to build a total
         * user score through a cron.
         */
        Schema::create('user_score_log', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->dateTime('created_at');

            $table->integer('profile_id');
            $table->integer('neighborhood_id');
            $table->integer('category_id');
            $table->integer('voted_by');

            // 1 = Up
            // 2 = Down
            // 3 = Share
            $table->tinyInteger('type');
            $table->integer('points');

            $table->index('profile_id');
            $table->index('voted_by');
            $table->index('neighborhood_id');
            $table->index('category_id');
        });


        /**
         * Basic overview of user statistics.
         */
        Schema::create('user_stats', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->dateTime('updated_at');

            $table->integer('profile_id');
            $table->integer('votes_up');
            $table->integer('votes_down');
            $table->integer('shares');
            $table->integer('logins');
            $table->integer('page_views');
            $table->integer('questions_asked');
            $table->integer('questions_answered');
            $table->integer('deals_added');
            $table->integer('deals_accepted');
            $table->integer('users_followed');
            $table->integer('users_following');

            $table->index('profile_id');
        });


        /**
         * List of who is following which profiles.
         */
        Schema::create('user_follows', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->timestamps();

            $table->integer('profile_id');
            $table->integer('follow_profile_id');

            // 1 = Following
            // 0 = Unfollowed
            // 2 = Hide from user's feed.
            $table->tinyInteger('status')->default('1');

            $table->index('profile_id');
            $table->index('following');
        });
    }

    /**
     * Reverse the migrations.
     *d
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
        Schema::drop('user_profile');
        Schema::drop('user_images');
        Schema::drop('user_privacy');
        Schema::drop('user_social');
        Schema::drop('user_devices');
        Schema::drop('user_score');
        Schema::drop('user_score_log');
        Schema::drop('user_stats');
        Schema::drop('user_follows');
    }
}
