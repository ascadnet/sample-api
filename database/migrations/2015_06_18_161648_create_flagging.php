<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlagging extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * List of users who have claimed a deal.
         */
        Schema::create('flags', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->timestamps();

            $table->integer('flagged_by');
            $table->integer('item_flagged');

            // 1 = User
            // 2 = Question
            // 3 = Answer
            // 4 = Deal
            $table->integer('item_type');

            $table->string('reason');

            $table->index('flagged_by');
            $table->index('item_flagged');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
