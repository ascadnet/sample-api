<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdjustments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('user_images', 'user_media');

        Schema::table('user_media', function (Blueprint $table) {
            // 1 = photo / 2 = video
            $table->tinyInteger('media_type')->default('1');

            $table->tinyInteger('format')->change();
        });

        Schema::table('user_follows', function (Blueprint $table) {
            $table->tinyInteger('follow_type')->after('updated_at');

            $table->integer('neighborhood_id')->after('follow_profile_id');
            $table->integer('city_id')->after('neighborhood_id');

            // $table->renameColumn('following', 'follow_profile_id');

            $table->index('neighborhood_id');
            $table->index('city_id');
        });

        Schema::table('user_experts', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->timestamps();

            $table->integer('profile_id');

            // 1 = Community / 2 = Industry
            $table->tinyInteger('type')->default('1');
            $table->integer('neighborhood_id');
            $table->integer('category_id');

            $table->index('profile_id');
            $table->index('neighborhood_id');
            $table->index('category_id');
        });

        Schema::table('user_profile', function (Blueprint $table) {

            // 1 = male / 2 = female
            $table->tinyInteger('gender')->default('');

        });

        Schema::table('user_follows', function (Blueprint $table) {

            // 1 = profile
            // 2 = neighborhood
            // 3 = question
            // 4 = service
            $table->tinyInteger('status')->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_experts');
    }
}
