<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaseCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::insert("
          INSERT INTO categories (id, sub_category, `name`, status, is_person) VALUES
            ('3', '', 'Automotive', '1', ''),
            ('4', '', 'Education', '1', '0'),
            ('5', '', 'Events', '1', '0'),
            ('6', '', 'Family & Parenting', '1', '0'),
            ('7', '', 'Fitness', '1', '0'),
            ('8', '', 'General Services & Stores', '1', '0'),
            ('9', '', 'Health', '1', '0'),
            ('10', '', 'Home', '1', '0'),
            ('12', '', 'Pets', '1', '0'),
            ('13', '', 'Professional Services', '1', '0'),
            ('14', '', 'Real Estate', '1', '0'),
            ('16', '', 'Shopping', '1', '0'),
            ('17', '', 'Beauty', '1', '0'),
            ('18', '', 'Recreation', '1', '0')
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
