<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        /**
         * List of locations for services.
         */
        Schema::create('geo_user', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->integer('profile_id');
            $table->integer('neighborhood_id');

            $table->string('address_line_1', 150);
            $table->string('address_line_2', 30);
            $table->string('city', 85);
            $table->string('state', 3);
            $table->string('zip', 10);

            $table->decimal('lat', 10, 6);
            $table->decimal('lon', 10, 6);

            $table->index('profile_id');
            $table->index('neighborhood_id');
        });


        /**
         *
         */
        Schema::create('geo_borough', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->string('name', 100);
            $table->string('state', 3);

            $table->timestamp('create_date');
            $table->timestamp('update_date');
            $table->string('created_by', 65);
            $table->string('updated_by', 65);
        });

        /**
         *
         */
        Schema::create('geo_metro', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->string('name', 50);
            $table->decimal('lat', 10, 6);
            $table->decimal('lon', 10, 6);

            // Will need to remove this eventually.
            $table->string('seo_ref');
        });

        /**
         *
         */
        Schema::create('geo_metro_borough', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('metro_id');
            $table->integer('borough_id');

            $table->timestamp('created_at');
            $table->string('created_by', 45);

            $table->index('metro_id');
            $table->index('borough_id');
        });


        /**
         *
         */
        Schema::create('geo_countries', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->string('name', 85);
            $table->string('abbreviation', 3);
        });


        DB::insert("
          INSERT INTO geo_countries (id, name, abbreviation) VALUES
            (1, 'United States', 'USA')
        ");

        /**
         * Manually import required:
         *
         * - geo_neighborhoods
         * - geo_neighborhood_polygons
         */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('geo_borough');
        Schema::drop('geo_user');
        Schema::drop('geo_metro');
        Schema::drop('geo_metro_borough');
        Schema::drop('geo_countries');
    }
}
