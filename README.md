# Romio API v2

## Requirements

The API is build using Laravel 5.1, which has the following requirements:

- PHP >= 5.5.9
- OpenSSL PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension

Additional requirements include:

- Elastisearch
- MongoDB

## Local

- Pull the develop branch.
- Run a "composer update" to download dependencies.
- Establish your environment variables within the .env file.
- Do not push the vendor directory to the repo!