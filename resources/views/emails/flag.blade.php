An {{ $type }} has been flagged:

<br /><br />
ITEM:<br />
@foreach ($item as $key => $value)
    {{ $key }}: {{ $value }}<br />
@endforeach

<br /><br />
FLAGGED BY:<br />
@foreach ($by as $key => $value)
    {{ $key }}: {{ $value }}<br />
@endforeach

